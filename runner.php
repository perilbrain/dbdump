<?php
/**
 * Author: perilbrain
 */

require_once  './core/core.php';
require_once './core/loader.php';

$mode = null;
$db_class = null;

function help($argv, $error){
    global $db_plugins, $processors;
    $error = !is_null($error) ? "Error: $error " : "";
    $allowed_dbs = implode(', ', $db_plugins->getActivePluginNames());
    $allowed_processors = implode(', ', $processors->getActivePluginNames());
    return "
$error    
    
Dump any database in simple format
Usage args: php ${argv[0]} -d < mode > [--option value] -- db_name
    
    where option could be:
    --prefix       [value] i.e. A common prefix in all table will be removed in dumping     
    --schema       [value] i.e. [pg specific] schema name default is [public] 
    --server       [value] i.e. ip address of server default is [localhost] 
    --port         [value] i.e. db server port default pg:5432, mysql: 3306 
    --username     [value] i.e. default pg: postgres, mysql: root 
    --password     [value] i.e. default pg: null, mysql: null  
    --processor    [value] i.e. template to use for dumping, default hibernate
    --config       [file]  i.e. file to configure the processor
    
    and 
    < mode > = $allowed_dbs
    
    processor could be $allowed_processors
    
    ex:
    php runner.php -d pg --processor=custom -- my_db_name > /tmp/my_db.rel
    php runner.php -d pg --server=\"10.2.21.54\" --username=\"postgres\" --password=\"hello_seattle\" --schema=\"public\" -- my_remote_db
    php runner.php -d mysql --config=/tmp/config.php -- some_db
    \n";
}

// parse commandline
$optind = null;
$opts = getopt('d:', ['prefix::', 'schema::', 'server::', 'port::', 'username::', 'password::', 'processor::', 'config::', 'help'], $optind);
$analyze_db = implode("", array_slice($argv, $optind));

$db_plugins = PluginLoader::loadAllPlainPlugins(["db"]);
$processors = PluginLoader::loadAllPlainPlugins(["processors"]);

if(isset($opts['help'])){
    echo help($argv, null);
    exit(0);
}
if(!isset($opts['d'])){
    echo help($argv, "No db vendor specified.");
    exit(1);
}

if (empty($analyze_db)) {
    echo help($argv, "No DB name supplied.");
    exit(1);
} else {
    echo "// Operation on database: $analyze_db\n";
}

$db_vendor = $opts['d'];
$db_plugin = $db_plugins->getPlugin($db_vendor);
if(is_null($db_plugin)){
    echo help($argv, "No valid mode, got $db_vendor");
    exit(1);
}

echo "// Using database vendor: {$db_plugin->getName()}\n";

// Get default parameters
$def = $db_plugin->configure(null);

$def = DBDefaults::parse_commands($db_plugin->getName(), $opts, $analyze_db, $def);


/** @var RelDB $db */
$db = $db_plugin->run($def);

$db->connect();
$db->initQuery();
print " \n// =========== Model for $analyze_db =============\n\n";
$db_store = $db->modelDB(0, ['schema' => $def->getDbSchema()]);

$runner = isset($opts['processor'])? $opts['processor'] : 'hibernate';

$processor = $processors->getPlugin($runner);
if(is_null($processor)){
    echo "Processor \"$runner\" is not available !!!";
    exit(1);
}
$processor->configure($opts);
$processor->run($db_store);


$db->close();
