<?php
namespace DBDump\DBPlugin\Mysql;

require_once  'core/db.php';
require_once  'core/db_defaults.php';
require_once  'core/plugin_interface.php';

use RelDB;
use Constraint;
use DataAttribute;
use Entity;
use SchemaPack;
use DBStore;
use ILoadablePlugin;
use DBDefaults;

/**
 *
 */
class MysqlDB extends RelDB {
    private $reverseDB = null;

    public static function get_db_defaults(){
        new DBDefaults('mysql', '3306', 'localhost',
            'root', '', '', null, null);
    }

    /**
     * MysqlDB constructor.
     * @param DBDefaults $def_db
     */
    public function __construct( $def_db) {
        parent::__construct($def_db);
        $this->reverseDB = $def_db->getSelectedDB();
    }


    public function connect() {
        $ref = $this->def_db;
        $this->bridge = new mysqli($ref->getServerAddress(), $ref->getUserName(), $ref->getPassword());
        if (!$this->bridge) {
            $this->bridge = null;
            die("// ** Connection failed: \n");
        }
    }

    public function close() {
        if (!is_null($this->bridge)) {
            $this->bridge->close();
        } else {
            $this->warnDisconnect();
        }
    }

    public function query($query = '') {
        if (!is_null($this->bridge)) {
            return $this->bridge->query($query);
        } else {
            $this->warnDisconnect();
            return null;
        }
    }

    private function objectFetch($query){
        $res = $this->query($query);
        $res_array = [];
        while ($row = pg_fetch_object($res)) {
            $res_array[] = $row;
        }
        return $res_array;
    }

    public function modelDB($synthetic_level=3, $filter=[]) {
        // Build the super db
        $db_store = new DBStore($filter);
        $db_store->setName($this->def_db->getSelectedDB());
        $db_store->setIsDecoy(true);

        foreach ($this->getAllSchemaPacks($db_store) as $schema){
            $schema->db_store = $db_store;
            foreach ($this->getAllEntities($schema) as $entity){
                $entity->schema_pack = $schema;
                $schema->entities[$entity->getName()] = $entity;
                foreach ($this->getAllAttributes($schema, $entity) as $attr){
                    $attr->entity = $entity;
                    $entity->attemptAdd($attr);
                }
            }
            $db_store->schema_packs[$schema->getName()] = $schema;
        }

        $this->setAllConstraints($db_store->getName(), $db_store);

        $this->discover_synthetic($db_store, $synthetic_level);

        $db_store->process();


        return $db_store;
    }

    public function discover_synthetic(&$db_store, $synthetic_level){

        $this->synthetic_match($db_store, $synthetic_level, []);


        if ($synthetic_level & 4 > 0){
            // perform_set_match
        }
    }


    /**
     * @param \DBStore $db_store
     * @return \SchemaPack[]
     */
    public function getAllSchemaPacks(&$db_store)
    {
        $build_schema = function($obj){
            return new SchemaPack(true, $obj->schema_name);
        };

        $schema_query = "";

        return array_map(
            $build_schema,
            $this->objectFetch($schema_query)
        );
    }


    /**
     * @param SchemaPack $schema_pack
     * @return Entity[]
     */
    public function getAllEntities(&$schema_pack)
    {
        $table_query = "";
        /**
         * @var Entity[] $entities
         */
        $entities = [];

        foreach ($this->objectFetch($table_query) as $obj) {
            $table_name = $obj->table_name;
            $normalized_name = $this->normalize_name($table_name);
            $entity = new \Entity($normalized_name, $table_name);
            $entities[$table_name] = $entity;
            if(!empty($obj->parent_table)) {
                $entity->parent_entity = $entities[$obj->parent_table];
                $entities[$obj->parent_table]->child_entities[] = $entity;
            }
        }

        return array_values($entities);
    }

    /**
     * @param \SchemaPack $schema_pack
     * @param \Entity $entity
     * @return \DataAttribute[]
     */
    public function getAllAttributes(&$schema_pack, &$entity)
    {
        $regex = '/nextval\(\'([A-z0-9_]+)\'::regclass\)/m';
        $build_attribute = function($object) use($regex) {
            $da = new DataAttribute();
            if($object->is_identity === 'YES'){
                $da->identity = new \StdClass();
            }
            foreach (get_object_vars($object) as $key => $value) {
                if($key === 'is_identity' || strpos($key, 'identity_') === 0){
                    if(!is_null($da->identity)) {
                        $new_key = str_replace('identity_', '', $key);
                        $da->identity->$new_key = $value;
                    }
                    continue;
                }
                if(is_bool($da->$key)){
                    $da->$key = $value == 'YES';
                } else {
                    $da->$key = $value;
                }
            }

            if (!empty($da->def_val) && preg_match_all($regex, $da->def_val, $matches, PREG_SET_ORDER, 0) > 0) {
                if (isset($matches[0][1])) {
                    $da->sequence = $matches[0][1];
                }
            }
            return $da;
        };

        $eff_parent = $entity->isDerived()? $entity->parent_entity->getName() : $entity->getName();

        // $query_text = "SELECT `COLUMN_NAME`, `COLUMN_DEFAULT`, `DATA_TYPE`, `TABLE_NAME`, `COLUMN_KEY` FROM `COLUMNS` WHERE `TABLE_SCHEMA` = '$db_name' ORDER BY `TABLE_NAME`, `ORDINAL_POSITION` ASC;";
        $attribute_query = "";

        return array_map($build_attribute, $this->objectFetch($attribute_query));
    }

    private function getColumnsFromStr($str, &$tb){
        $filtered = array_filter(explode(',', $str), function ($x){ return !empty($x);});
        return array_map(function ($col) use ($tb) {
            return $tb->data_attributes[$col];
        }, $filtered) ;
    }

    /**
     * @param string $resident_name
     * @param DBStore $db_store
     */
    public function setAllConstraints($resident_name, &$db_store)
    {

        $valid_tables = implode(',' , array_map(function ($x){ return "'$x'"; }, array_column($db_store->getEntities(), 'tb_name')));

        // $foreign_query = "SELECT `TABLE_NAME`, `COLUMN_NAME`, `REFERENCED_TABLE_NAME`, `REFERENCED_COLUMN_NAME` FROM `KEY_COLUMN_USAGE` WHERE `TABLE_SCHEMA` = '$db_name' AND `TABLE_SCHEMA` = `REFERENCED_TABLE_SCHEMA` AND (`REFERENCED_TABLE_NAME` IS NOT NULL AND `REFERENCED_COLUMN_NAME` IS NOT NULL)";
        $constraint_query = "";

        /**
         * @var Constraint[] $constraints
         */
        $constraints = [];

        foreach ($this->objectFetch($constraint_query) as $object){
            $new_c = null;
            $resident_schema = $db_store->schema_packs[$object->resident_schema];
            $resident_table = $resident_schema->entities[$object->resident_table];
            $resident_columns = $this->getColumnsFromStr($object->resident_columns, $resident_table);
            switch ($object->constraint_type){
                case 'FOREIGN KEY':
                    $foreign_schema = $db_store->schema_packs[$object->foreign_schema];
                    $foreign_table = $foreign_schema->entities[$object->foreign_table];
                    $foreign_columns = $this->getColumnsFromStr($object->foreign_columns, $foreign_table);
                    $cs = $db_store->createForeignConstraintEx($object->constraint_name, $resident_columns, $resident_table,
                        $foreign_columns, $foreign_table, false);
//                    $cs = new ForeignConstraint(
//                        $object->constraint_name, $resident_schema,
//                        $resident_table, $resident_columns,
//                        Constraint::FOREIGN, $foreign_schema,
//                        $foreign_table, $foreign_columns);
//                    $resident_table->addForeign($cs);
                    $constraints[] = $cs;
                    break;
                case 'PRIMARY KEY':
                    $cs = new Constraint(
                        $object->constraint_name, $resident_schema,
                        $resident_table, $resident_columns,
                        Constraint::PRIMARY);
                    $resident_table->setPrimaryConstraint($cs);
                    $constraints[] = $cs;
                    break;
                case 'UNIQUE KEY':
                case 'UNIQUE':
                    $cs = new Constraint(
                        $object->constraint_name, $resident_schema,
                        $resident_table, $resident_columns,
                        Constraint::UNIQUE);
                    $resident_table->addUnique($cs);
                    $constraints[] = $cs;
                    break;
                default:
                    $cs = new Constraint(
                        $object->constraint_name, $resident_schema,
                        $resident_table, $resident_columns,
                        $object->constraint_type);
                    $resident_table->addConstraint($cs);
                    $constraints[] = $cs;
                    break;
            }
        }

        foreach ($constraints as $constraint) {
            $constraint->process();
        }


    }
}

class ExternalPlugin implements \DBPlugin {

    public function getName()
    {
        return 'mysql';
    }

    public function configure($params=null)
    {
        return MysqlDB::get_db_defaults();
    }

    public function run($params)
    {
        throw new \RuntimeException("Not yet implemented");
        // return new MysqlDB($params);
    }

    public function clean($params)
    {
        // TODO: Implement clean() method.
    }

    public function help()
    {
        // TODO: Implement help() method.
    }

    public function isFunctional()
    {
        return false;
    }
}


return __NAMESPACE__;
