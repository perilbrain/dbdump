<?php
namespace DBDump\DBPlugin\Postgres;

require_once  'core/db.php';
require_once  'core/db_defaults.php';
require_once  'core/plugin_interface.php';

use RelDB;
use Constraint;
use DataAttribute;
use Entity;
use SchemaPack;
use DBStore;
use ILoadablePlugin;
use DBDefaults;

/**
 *
 */
class PGDB extends RelDB {
    private $reverseDB = null;
    private $version;

    public static function get_db_defaults(){
        return new DBDefaults('postgres', '5432', 'localhost',
            'postgres', '', '', 'public', null);
    }

    /**
     * PGDB constructor.
     * @param DBDefaults $def_db
     */
    public function __construct( $def_db) {
        parent::__construct($def_db);
        $this->reverseDB = $def_db->getSelectedDB();
    }


    public function connect() {
        $ref = $this->def_db;
        $connection_string = implode(" ",
        array_map(function ($k, $v){
                return "$k=$v";
            },
            array("host", "port", "dbname", "user", "password"),
            array($ref->getServerAddress(), $ref->getPort(), $ref->getSelectedDB(), $ref->getUserName(), $ref->getPassword())
        ));
        $this->bridge = pg_connect($connection_string);
        if (!$this->bridge) {
            $this->bridge = null;
            die("// ** Connection failed: \n");
        }

        $this->version = pg_version($this->bridge)['server'];
    }

    public function close() {
        if (!is_null($this->bridge)) {
            pg_close($this->bridge);
        } else {
            $this->warnDisconnect();
        }

    }

    public function query($query = '') {
        if (!is_null($this->bridge)) {
            return pg_query($this->bridge, $query);
        } else {
            $this->warnDisconnect();
            return null;
        }
    }

    private function objectFetch($query){
        $res = $this->query($query);
        $res_array = [];
        while ($row = pg_fetch_object($res)) {
            $res_array[] = $row;
        }
        return $res_array;
    }

    public function modelDB($synthetic_level=3, $filter=[]) {
        // Build the super db
        $db_store = new DBStore($filter);
        $db_store->setName($this->def_db->getSelectedDB());
        $db_store->setIsDecoy(false);

        foreach ($this->getAllSchemaPacks($db_store) as $schema){
            $schema->db_store = $db_store;
            foreach ($this->getAllEntities($schema) as $entity){
                $entity->schema_pack = $schema;
                $schema->entities[$entity->getName()] = $entity;
                foreach ($this->getAllAttributes($schema, $entity) as $attr){
                    $attr->entity = $entity;
                    $entity->attemptAdd($attr);
                }
            }
            $db_store->schema_packs[$schema->getName()] = $schema;
        }

        $this->setAllConstraints($db_store->getName(), $db_store);

        $this->discover_synthetic($db_store, $synthetic_level);

        $db_store->process();


        return $db_store;
    }

    public function discover_synthetic(&$db_store, $synthetic_level){

        $this->synthetic_match($db_store, $synthetic_level, ['bigint', 'integer', 'smallint']);


        if ($synthetic_level & 4 > 0){
            // perform_set_match
        }
    }


    /**
     * @param \DBStore $db_store
     * @return \SchemaPack[]
     */
    public function getAllSchemaPacks(&$db_store)
    {
        $build_schema = function($obj){
            return new SchemaPack(true, $obj->schema_name);
        };

        $catalog_name = $db_store->getName();

        $extra = is_null($db_store->only_schema)?"" : "AND t.schema_name='{$db_store->only_schema}'";

        $schema_query = "SELECT t.schema_name FROM information_schema.schemata AS t 
            WHERE t.schema_name NOT LIKE 'pg_%' AND t.schema_name <> 'information_schema' 
            AND t.catalog_name = '$catalog_name' $extra;";

        return array_map(
            $build_schema,
            $this->objectFetch($schema_query)
        );
    }


    /**
     * @param SchemaPack $schema_pack
     * @return Entity[]
     */
    public function getAllEntities(&$schema_pack)
    {
        $schema_name = $schema_pack->getName();
        $extra = "";
        if (version_compare($this->version, '10.0') >= 0) {
            $extra .= " AND pc.relispartition=false";
        }

        $table_query = "SELECT pc.relname as table_name, pij.parent as parent_table, 
                obj_description(pc.oid) as table_comment
            FROM pg_catalog.pg_class pc
            JOIN pg_namespace pn ON pc.relnamespace = pn.oid
            JOIN information_schema.tables tb ON tb.table_name = pc.relname 
              AND tb.table_type='BASE TABLE' AND pn.nspname=tb.table_schema
            LEFT JOIN ( 
                SELECT pc1.relname as parent, pi.inhrelid FROM pg_inherits pi
                JOIN pg_catalog.pg_class pc1 ON pc1.oid=pi.inhparent
            ) pij ON pc.oid=pij.inhrelid
            WHERE tb.table_schema='$schema_name' $extra
            ORDER BY parent_table DESC, table_name;";
        /**
         * @var Entity[] $entities
         */
        $entities = [];

        foreach ($this->objectFetch($table_query) as $obj) {
            $table_name = $obj->table_name;
            $normalized_name = $this->normalize_name($table_name);
            $entity = new \Entity($normalized_name, $table_name);
            if(!empty($obj->table_comment))
                $entity->table_comment = $obj->table_comment;
            $entities[$table_name] = $entity;
            if(!empty($obj->parent_table)) {
                $entity->parent_entity = $entities[$obj->parent_table];
                $entities[$obj->parent_table]->child_entities[] = $entity;
            }
        }

        return array_values($entities);
    }

    /**
     * @param \SchemaPack $schema_pack
     * @param \Entity $entity
     * @return \DataAttribute[]
     */
    public function getAllAttributes(&$schema_pack, &$entity)
    {
        $regex = '/nextval\(\'([A-z0-9_]+)\'::regclass\)/m';
        $build_attribute = function($object) use($regex) {
            $da = new DataAttribute();
            if($object->is_identity === 'YES'){
                $da->identity = new \StdClass();
            }
            foreach (get_object_vars($object) as $key => $value) {
                if($key === 'is_identity' || strpos($key, 'identity_') === 0){
                    if(!is_null($da->identity)) {
                        $new_key = str_replace('identity_', '', $key);
                        $da->identity->$new_key = $value;
                    }
                    continue;
                }
                if(is_bool($da->$key)){
                    $da->$key = $value == 'YES';
                } else {
                    $da->$key = $value;
                }
            }

            if (!empty($da->def_val) && preg_match_all($regex, $da->def_val, $matches, PREG_SET_ORDER, 0) > 0) {
                if (isset($matches[0][1])) {
                    $da->sequence = $matches[0][1];
                }
            }
            return $da;
        };

        $schema_name = $schema_pack->getName();

        $eff_parent = $entity->isDerived()? $entity->parent_entity->getName() : $entity->getName();

        $attribute_query = "SELECT c.column_name as name, c.column_default as def_val, c.data_type as dtype,
            col_description((c.table_schema||'.'||c.table_name)::regclass::oid, c.ordinal_position) as comment, 
            c.is_nullable , c.is_updatable , c.character_maximum_length, c.numeric_precision, c.numeric_precision_radix,
            c.is_generated, c.generation_expression, 
            c.is_identity, c.identity_generation, c.identity_start, c.identity_increment, c.identity_maximum, c.identity_minimum, c.identity_cycle
            FROM information_schema.columns AS c 
            LEFT JOIN information_schema.columns c2 
              ON c.column_name = c2.column_name  
                AND c.data_type = c2.data_type 
                AND c.table_schema = c2.table_schema
                AND c2.table_name = '$eff_parent'
            WHERE c.table_name='$entity->tb_name' AND c.table_schema='$schema_name'
              AND (c2.table_name = '$entity->tb_name' OR c2.table_name is NULL)
            ORDER BY c.ordinal_position, c.column_name;";

        return array_map($build_attribute, $this->objectFetch($attribute_query));
    }

    private function getColumnsFromStr($str, &$tb){
        $filtered = array_filter(explode(',', $str), function ($x){ return !empty($x);});
        return array_map(function ($col) use ($tb) {
            return $tb->data_attributes[$col];
        }, $filtered) ;
    }

    /**
     * @param string $resident_name
     * @param DBStore $db_store
     */
    public function setAllConstraints($resident_name, &$db_store)
    {

        $extra = is_null($db_store->only_schema)?"" : "AND tc.resident_schema='{$db_store->only_schema}'";
        $valid_tables = implode(',' , array_map(function ($x){ return "'$x'"; }, array_column($db_store->getEntities(), 'tb_name')));

        $constraint_query = "SELECT
                tc.resident_schema,
                tc.constraint_type,
                tc.constraint_name, 
                tc.resident_table, 
                tc.resident_columns, 
                ccu.foreign_schema,
                ccu.foreign_table,
                ccu.foreign_columns 
            FROM 
                (
					SELECT
						MAX(tc1.table_catalog) as resident_catalog,
						MAX(tc1.table_schema) AS resident_schema, 
						MAX(tc1.table_name) AS resident_table,
						MAX(tc1.constraint_type) AS constraint_type,
						tc1.constraint_name,
						string_agg(kcu.column_name, ',' ORDER BY kcu.column_name) AS resident_columns
					FROM information_schema.table_constraints AS tc1
					JOIN information_schema.key_column_usage AS kcu
						ON tc1.constraint_name = kcu.constraint_name
						AND tc1.table_schema = kcu.table_schema
					GROUP BY tc1.constraint_name
				) AS tc
                JOIN (
				SELECT 
					MAX(table_catalog) as foreign_catalog, 
					MAX(table_schema) as foreign_schema, 
					MAX(table_name) as foreign_table, 
					string_agg(column_name, ',' ORDER BY column_name) AS foreign_columns, 
					MAX(constraint_catalog) as constraint_catalog, 
					MAX(constraint_schema) as constraint_schema, constraint_name
				FROM information_schema.constraint_column_usage
				GROUP BY constraint_name) AS ccu
                  ON ccu.constraint_name = tc.constraint_name
                  AND ccu.foreign_schema = tc.resident_schema
            WHERE tc.resident_catalog = '$resident_name' 
                AND tc.resident_table IN ($valid_tables)  
                AND ccu.foreign_table IN ($valid_tables)  
                $extra 
            ORDER BY tc.resident_table, tc.constraint_type DESC, tc.constraint_name, tc.resident_columns, ccu.foreign_schema,
                ccu.foreign_table, ccu.foreign_columns;";

        /**
         * @var Constraint[] $constraints
         */
        $constraints = [];

        foreach ($this->objectFetch($constraint_query) as $object){
            $new_c = null;
            $resident_schema = $db_store->schema_packs[$object->resident_schema];
            $resident_table = $resident_schema->entities[$object->resident_table];
            $resident_columns = $this->getColumnsFromStr($object->resident_columns, $resident_table);
            switch ($object->constraint_type){
                case 'FOREIGN KEY':
                    $foreign_schema = $db_store->schema_packs[$object->foreign_schema];
                    $foreign_table = $foreign_schema->entities[$object->foreign_table];
                    $foreign_columns = $this->getColumnsFromStr($object->foreign_columns, $foreign_table);
                    $cs = $db_store->createForeignConstraintEx($object->constraint_name, $resident_columns, $resident_table,
                        $foreign_columns, $foreign_table, false);
//                    $cs = new ForeignConstraint(
//                        $object->constraint_name, $resident_schema,
//                        $resident_table, $resident_columns,
//                        Constraint::FOREIGN, $foreign_schema,
//                        $foreign_table, $foreign_columns);
//                    $resident_table->addForeign($cs);
                    $constraints[] = $cs;
                    break;
                case 'PRIMARY KEY':
                    $cs = new Constraint(
                        $object->constraint_name, $resident_schema,
                        $resident_table, $resident_columns,
                        Constraint::PRIMARY);
                    $resident_table->setPrimaryConstraint($cs);
                    $constraints[] = $cs;
                    break;
                case 'UNIQUE KEY':
                case 'UNIQUE':
                    $cs = new Constraint(
                        $object->constraint_name, $resident_schema,
                        $resident_table, $resident_columns,
                        Constraint::UNIQUE);
                    $resident_table->addUnique($cs);
                    $constraints[] = $cs;
                    break;
                default:
                    $cs = new Constraint(
                        $object->constraint_name, $resident_schema,
                        $resident_table, $resident_columns,
                        $object->constraint_type);
                    $resident_table->addConstraint($cs);
                    $constraints[] = $cs;
                    break;
            }
        }

        foreach ($constraints as $constraint) {
            $constraint->process();
        }


    }
}

class ExternalPlugin implements \DBPlugin {

    public function getName()
    {
        return 'pg';
    }

    public function configure($params=null)
    {
        return PGDB::get_db_defaults();
    }

    public function run($params)
    {
        return new PGDB($params);
    }

    public function clean($params)
    {
        // TODO: Implement clean() method.
    }

    public function help()
    {
        // TODO: Implement help() method.
    }

    public function isFunctional()
    {
        return true;
    }
}


return __NAMESPACE__;
