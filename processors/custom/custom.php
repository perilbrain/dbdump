<?php

namespace DBDump\Processor\Custom;

use ForeignConstraint;
use Constraint;
use DataAttribute;
use Entity;
use SchemaPack;
use DBStore;
use ILoadablePlugin;

function typeToStr($value) {

    if (!is_bool($value))
        return $value;
    return $value ? 'true':'false';
}

/**
 * @param DataAttribute[] $das
 */
function printAttributes($das){
    foreach ($das as $da) {
        echo "\t{$da->name} (";
        $props = ["Primary" => $da->is_primary, "T" => $da->dtype, "D" => $da->def_val, "L" => $da->isLink(),
            "Unique" => $da->is_unique, "Nullable" => $da->is_nullable, "Updatable" => $da->is_updatable];

        $prop_def = ["Primary" => false, "T" => null, "D" => null, "L" => false,
            "Unique" => false, "Nullable" => true, "Updatable" => true];
        $new_props = [];
        foreach ($props as $key => $val) {
            if($val === $prop_def[$key])
                continue;
            $str_val = typeToStr($val);
            $new_props[] = "\"$key\": \"$str_val\"";
        }
        /**
         * @var DataAttribute $da
         */
        if($da->isLink()){
            /**
             * @var DataAttribute $ref_attr
             */
            $ref_attr = $da->foreign_constraint->foreign_columns[0];
            $satellite = $ref_attr->getParent();
            $qualified_name = "{$satellite->getParent()->getName()}.{$satellite->name}.{$ref_attr->name}";
            $new_props[] = "\"ref\": $qualified_name, \"ref_type\": \"{$da->foreign_constraint->getLinkType()}\"";
        }
        echo implode(", ", $new_props);

        echo ")\n";
    }
}

/**
 * @param Entity[] $entities
 */
function printRelations($entities){

    echo "\n// Relations:\n\n";

    // Dump link tables first
    /**
     * @var Entity[] $link_entities
     */
    $link_entities = array_filter($entities, function (Entity $entity){ return $entity->seemsLink();});
    /**
     * @var Entity[] $in_link_entities
     */
    $in_link_entities = array_filter($entities, function (Entity $entity){
        return !$entity->seemsLink() && $entity->hasInLinks();
    });

    // Dump link table
    foreach ($link_entities as $link_entity) {
        foreach ($link_entity->link_entity_records as $entity_record){
            /**
             * @var LinkEntityRecord $entity_record
             */
            $left_entity = $entity_record->left_fc->foreign_table;
            $right_entity = $entity_record->right_fc->foreign_table;

            $left_attrs = implode(', ', array_column($entity_record->left_fc->resident_columns, 'name'));
            $r_left_attrs = implode(', ', array_column($entity_record->left_fc->foreign_columns, 'name'));
            $r_right_attrs = implode(', ', array_column($entity_record->right_fc->foreign_columns, 'name'));
            $right_attrs = implode(', ', array_column($entity_record->right_fc->resident_columns, 'name'));

            echo "\n#{$left_entity->getFullName()}({$left_attrs})\t";
            echo "<{$entity_record->rel_type}( by: #{$link_entity->getFullName()}";
            echo "[($r_left_attrs), ($r_right_attrs)] )>\t";
            echo "#{$right_entity->getFullName()}({$right_attrs})\n";
        }
    }

    // Dump individual
    foreach ($in_link_entities as $in_link_entity) {
        /**
         * @var ForeignConstraint[] $eff_in_bound
         */
        $eff_in_bound = array_filter($in_link_entity->in_bound_fc, function (ForeignConstraint $fc){ return !$fc->resident_table->seemsLink(); });
        if(empty($eff_in_bound))
            continue;
        $per_key_cs = [];
        // agg these by key
        foreach ($eff_in_bound as $fc) {
            $key = implode(', ', array_column($fc->foreign_columns, 'name'));
            // no need to check in PHP
            $per_key_cs[$key][] = $fc;
        }

        foreach ($per_key_cs as $key => $fc_list) {
            /**
             * @var ForeignConstraint[] $fc_list
             */
            echo "\n#{$in_link_entity->getFullName()}({$key})\t";
            $rels = [];
            foreach ($fc_list as $fc) {
                $res_key = implode(', ', array_column($fc->resident_columns, 'name'));
                $desc = "<{$fc->getLinkType()} by:({$res_key})>";
                // no need to check in PHP
                $rels[$desc][] = "#{$fc->resident_table->getFullName()}";
            }

            $first = true;
            foreach ($rels as $desc => $e_arr) {
                if(!$first)
                    echo "\t\t\t\t\t";
                $f_val = count($e_arr) > 1 ? "{ ".implode(", ", $e_arr)." }" : $e_arr[0];
                echo "$desc $f_val\n";
                $first = false;
            }


        }
    }
}

/**
 * @param Entity[] $entities
 */
function printEntities($entities){

    echo "// Entities:\n\n";
    foreach ($entities as $entity) {
        $parent = $entity->isDerived()? "( ".$entity->parent_entity->getFullName()." )" : "";
        echo "#{$entity->getFullName()} $parent {\n";
        printAttributes($entity->getDataAttributes());
        echo "}\n";
    }
}

function printConstraints($constraints){
    echo "\n// Constraints:\n\n";
    /**
     * @var ForeignConstraint $constraint
     */
    $ct = ['1' => 'primary_key', '2' => 'unique_key', '3' => 'foreign_key'];
    foreach ($constraints as $constraint) {
        $cols = implode(', ', $constraint->getResidentNames(true));
        $cn = $ct["{$constraint->constraint_type}"];
        if($constraint->is_synthetic)
            echo "\n";
        echo "@{$constraint->constraint_name}(\"T\": \"$cn\", \"Col\": ($cols)";
        if($constraint->constraint_type === Constraint::FOREIGN){
            $fcols = implode(', ', $constraint->getForeignNames(true));
            echo ", \"F\": ($fcols)";
        }
        echo ")\n";
    }
}

/**
 * @param DBStore $db_store
 */
function template_process($db_store){
    $entities = $db_store->getEntities();
    $constraints = $db_store->getConstraints();
    usort($entities, function($a, $b) {return strcmp($a->name, $b->name);});
    usort($constraints, function($a, $b) {
        $f = $a->constraint_type - $b->constraint_type;
        return $f != 0? $f: strcmp($a->constraint_name, $b->constraint_name);});
    printEntities($entities);
    printRelations($entities);
    printConstraints($constraints);
    return true;

}

class ExternalPlugin implements ILoadablePlugin{

    public function getName()
    {
        return 'custom';
    }

    public function configure($params=null)
    {
    }

    public function run($params)
    {
        return template_process($params);
    }

    public function clean($params)
    {

    }

    public function help()
    {
        // TODO: Implement help() method.
    }
}

return __NAMESPACE__;