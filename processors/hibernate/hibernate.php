<?php

namespace DBDump\Processor\Hibernate;
require_once 'core/core.php';
require_once 'configuration.php';
require_once 'entity_gen.php';
require_once 'pg_hacks.php';

use ForeignConstraint;
use Constraint;
use DataAttribute;
use Entity;
use SchemaPack;
use DBStore;
use ILoadablePlugin;



// @TODO: https://stackoverflow.com/questions/31385658/jpa-how-to-make-composite-foreign-key-part-of-composite-primary-key
// unique keys, foreign keys, relations

class Hibernate{
    public $db_vendor;
    public $opt;
    /**
     * @var Configuration $config
     */
    public $config;


    public static function camelCase($str, $first_capital=false, array $no_strip = [])
    {
        return Configuration::camelCase($str, $first_capital, $no_strip);
    }
    public static function tableToClass($str){ return Configuration::camelCase($str, true); }
    public static function columnToAttr($str){ return Configuration::camelCase($str); }
    public static function q($str){ return "\"$str\""; }

    public static function entitySort(array $entities)
    {
        // Process parents first
        // a < b => r < 0;
        // a == b => r == 0;
        // a > b => r > 0;
        usort($entities, function(Entity $a, Entity $b) {
            $c = $a->isParent() == $b->isParent();
            // tackle both false
            if($c){
                if($a->isParent()){
                    $d = $a->parent_entity->isParent() === $b->parent_entity->isParent();
                    if(!$d)
                        return !$a->parent_entity->isParent() - !$b->parent_entity->isParent();
                }
                return strcmp($a->name, $b->name);
            }
            // if a is parent return <0 (a before b), if b is parent return >0
            return !$a->isParent() - !$b->isParent();
        });
        return $entities;
    }

    public function configure($opt){
        $this->opt = $opt;
        $this->db_vendor = $opt['d'];
        $this->config = new Configuration();
        if(isset($opt['config'])){
            $config_file = $opt['config'];
            $this->config->updateFromFile($config_file);
        }
        if(!$this->config->isConsole() && !is_dir($this->config->getOutDir())) {
            mkdir($this->config->getOutDir(), 0777, true);
        }
        $this->resolveTypeMaps();
    }

    public function getPackage(string $package)
    {
        return "{$this->config->getPackage()}.$package";
    }

    private function resolveTypeMaps()
    {
        $hook = $this->db_vendor.'_setup_typeMap';
        call_user_func_array(__NAMESPACE__.'\\'.$hook, array(&$this->config));
    }


    /**
     * Entry point of plugin, with reference of DBStore jungle.
     * @param DBStore $db_store
     * @return bool
     */
    public function template_process(DBStore $db_store){
        $entities = $db_store->getEntities();
        // ignore empty tables
        $filtered = array_filter($entities, function (Entity $entity){ return !empty($entity->data_attributes);});
        $built_classes = $this->processEntities($filtered);
        $this->processRepo($built_classes);
        $this->processUserTypes($built_classes);
        return true;
    }

    /**
     * Process entities to seclude keys, constraints and relations
     * @param Entity[] $entities
     * @return array
     */
    public function processEntities(array $entities){

        $entities = self::entitySort($entities);

        $built_classes = [];
        $mapped_by = [];
        foreach ($entities as $entity) {
            $tmp = new \StdClass();
            $tmp->entity = $entity;
            $tmp->primary_class = $this->getPrimaryClass($entity);
            $tmp->main_class = $this->getMainClass($entity, $mapped_by, $tmp->primary_class);
            $tmp->main_class_name = $tmp->main_class->name;
            $built_classes[$entity->getName()] = $tmp;
        }

        foreach ($built_classes as $entity_name => $built_class) {
            if($this->config->isActuallyBackReference())
                $built_class->main_class = $this->refineMainClass($built_class->entity, $built_class->main_class, $mapped_by, $built_class->primary_class);
            else
                $built_class->main_class->buildImports();
        }

        foreach ($built_classes as $entity_name => $built_class) {
            /**
             * @var Cls $cls
             */
            $cls = $built_class->main_class;
            if($cls->entity->isDerived()){
                $parent_cls = $built_classes[$cls->entity->parent_entity->getName()]->main_class;
                $cls->copyAttributes($parent_cls, true);
            }
            $this->dumpClass($built_class->primary_class);
            $this->dumpClass($built_class->main_class);
        }

        return $built_classes;
    }

    /**
     * Set up Objects for java specific class declarations and also dump them as proper java classes.
     * @param array $built_classes
     */
    public function processRepo(array &$built_classes)
    {
        foreach ($built_classes as $entity_name => &$built_class) {
            /**
             * @var Cls $cls
             */
            $cls = $built_class->main_class;
            $repo = Repo::buildFromCls($cls, $built_class->primary_class, $this->getPackage($this->config->getRepositoryDir()));
            $this->dumpAny($repo->getClassFileName(), $this->config->getRepositoryDir(), 'repo_dec', [
                'cls' => $repo
            ]);
            $built_class->repo = $repo;
        }
    }

    public function packageAsCreatedPath(string $package){
        $dir_path = implode(DIRECTORY_SEPARATOR, explode('.', $package));
        if(!is_dir($dir_path)) {
            mkdir($dir_path, 0777, true);
        }
        return $dir_path;
    }

    public function processUserTypes(array &$built_classes)
    {
        /**
         * @var TypeDetail[] $td_list
         */
        $td_list = [];
        foreach ($built_classes as $entity_name => &$built_class) {
            foreach ([$built_class->main_class, $built_class->primary_class] as $cls) {
                if(is_null($cls))
                    continue;
                foreach ($cls->getAllTypeDetails() as $type_detail) {
                    $td_list[] = $type_detail;
                }
            }
        }
        /**
         * @var TypeDetail[] $tl
         */
        $tl = array_unique($td_list);
        $dir_path = $this->packageAsCreatedPath($this->config->getTypeDir());
        foreach ($tl as $type_detail){
            $this->dumpAny($type_detail->getClassFileName(), $dir_path, 'attrib_dec', [
                'td' => $type_detail
            ]);
        }
    }

    private function resolveType(Cls $cls, Attrib &$attrib, DataAttribute $dataAttribute)
    {
        $hook = $this->db_vendor.'_process_attribute';
        call_user_func_array(__NAMESPACE__.'\\'.$hook, array(&$cls, &$attrib, $dataAttribute, $this->config));
    }

    /**
     * Extract composite primary key as a class.
     * @param Entity $entity
     * @return Cls|null
     */
    private function getPrimaryClass(Entity $entity)
    {
        if(is_null($entity->primary_constraint) || !$entity->primary_constraint->isComposite())
            return null;
        $cls = Cls::buildFromEntity($entity, self::tableToClass($entity->name).'PK', $this->getPackage($this->config->entity_dir));
        $cls->is_entity = false;
        if($this->config->isUseEmbed()){
            $cls->annots[] = new Annotation("Embeddable");
        }
        foreach ($entity->primary_constraint->resident_columns as $resident_column) {
            $attr = Attrib::buildFromDataAttribute($resident_column, self::columnToAttr($resident_column->name), $cls, $this->config);
            $this->resolveType($cls, $attr, $resident_column);
            $cls->attrs[] = $attr;
        }
        $cls->imports[] = "java.io.Serializable";
        $cls->buildImports();
        return $cls;
    }

    /**
     * @param Entity $entity
     * @param Cls|null $primary_class
     * @return Cls
     */
    private function getMainClass(Entity $entity, array &$mapped_by, Cls $primary_class=null)
    {
        $cls = Cls::buildFromEntity($entity, self::tableToClass($entity->name), $this->getPackage($this->config->getEntityDir()));
        if(!is_null($primary_class)) {
            $cls->id_class = $primary_class;
            $cls->id_attr = $primary_class;
            if(!$this->config->isUseEmbed())
                $cls->annots[]  = Annotation::fromArray("IdClass", ["{$primary_class->name}.class"]);
        }
        $cls->annots[] = new Annotation("Entity");

        // Declare @Table
        $annot = new Annotation("Table");
        $annot->kv_arr[] = KV::build('name', "\"{$entity->getName()}\"");
        if(!is_null($cls->schema_name))
            $annot->kv_arr[] = KV::build('schema' , "\"$cls->schema_name\"");
        if($this->config->isUseCatalog())
            $annot->kv_arr[] = KV::build('catalog' , "\"$cls->db_name\"");
        // Declare UniqueConstraint inside @Table
        $uniques = [];
        foreach ($entity->unique_constraints as $unique_constraint) {
            if(!$unique_constraint->isComposite())
                continue;
            $cols = array_map([__CLASS__, 'q'], array_column($unique_constraint->resident_columns, 'name'));
            $uniques[] = Annotation::fromArray("UniqueConstraint",
                ["columnNames" => $cols, "name" => self::q($unique_constraint->constraint_name)]);
        }
        if(!empty($uniques)) {
            $annot->kv_arr[] = KV::build('uniqueConstraints', $uniques);
        }
        $cls->annots[] = $annot;

        // Declare @Inheritance
        if($entity->isParent()) {
            $cls->annots[] = Annotation::fromArray("Inheritance", ["strategy" => "InheritanceType.TABLE_PER_CLASS"]);
        }

        $dumped_attributes = [];

        // get primary attribute first, if embedded replace with primary class, else direct process
        // if inherited use parent attributes
        if($this->config->isUseEmbed() && !is_null($primary_class)){
            $attr = new Attrib();
            $attr->name = self::columnToAttr($primary_class->name);
            $attr->type = $primary_class->name;
            $attr->annots[] = new Annotation("EmbeddedId");
            $cls->attrs[] = $attr;
        } else {
            foreach ($entity->getKeyAttributes(true, false, false) as $dataAttribute) {
                $attr = Attrib::buildFromDataAttribute($dataAttribute, self::columnToAttr($dataAttribute->name), $cls, $this->config);
                $this->resolveType($cls, $attr, $dataAttribute);
                $cls->attrs[] = $attr;
                if($dataAttribute->is_primary)
                    $cls->id_attr = $attr;
                $dumped_attributes[] = $dataAttribute->name;
            }
        }

        // Optionally create @X-to-Y mapping depending upon relation resolution allowed status.
        // get foreignKeys attribute, find reference and direct dump for any case.
        // This will put foreign keys as reference Entity.
        // Add self name as used, otherwise it wont make sense
        $used_names = [self::columnToAttr($entity->name)];
        if($this->config->isIgnoreRelations()){
            $foreign_das = $this->foreignAsNormalAttribute($entity, $cls);
            $dumped_attributes = array_merge($dumped_attributes, array_column($foreign_das, 'name'));
        } else {
            if (!$entity->is_link_entity) {
                foreach ($entity->foreign_constraints as $constraint) {

                    $ref_entity_type = self::tableToClass($constraint->foreign_table->name);
                    $attr = new Attrib();
                    $attr->name = self::getForeignAttrName($ref_entity_type, $constraint->resident_columns, $used_names);
                    $attr->type = $ref_entity_type;
                    // even if single is_nullable, optional becomes true
                    $is_optional = count(array_filter(array_column($constraint->resident_columns, 'is_nullable'), function ($x) {
                            return $x === true;
                        })) > 0;
                    $attr->annots[] = Annotation::fromArray($constraint->getLinkTypeString(true),
                        ['optional' => $is_optional ? 'true' : 'false', 'fetch' => 'FetchType.LAZY']);
                    $mapped_by["{$entity->getName()}.{$constraint->constraint_name}"] = $attr->name;
                    // @TODO: what if foreign is part of primary
                    $joins = self::joinColumns($constraint);
                    if (count($joins) > 1) {
                        $j_annot = new Annotation("JoinColumns");
                        $j_annot->sub_annots = $joins;
                        $attr->annots[] = $j_annot;
                    } else {
                        $attr->annots[] = $joins[0];
                    }
                    $cls->attrs[] = $attr;
                    $dumped_attributes = array_merge($dumped_attributes, array_column($constraint->resident_columns, 'name'));
                }
            }
        }

        //All others.
        $others = array_unique(array_merge($entity->getNonKeyAttributes(), $entity->getKeyAttributes(false, true, false)));
        foreach ($others as $dataAttribute) {
            if(!in_array($dataAttribute->name, $dumped_attributes)) {
                $this->normalAttrib($dataAttribute, $cls);
                $dumped_attributes[] = $dataAttribute->name;
            }
        }

        return $cls;
    }

    private function foreignAsNormalAttribute(Entity $entity, Cls& $cls){
        $foreign_das = [];
        foreach ($entity->foreign_constraints as $constraint){
            $foreign_das = array_merge($foreign_das, $constraint->resident_columns);
        }
        // Additionally add these attributes as normal so that hibernate saving works
        foreach ($foreign_das as $foreign_da) {
            $this->normalAttrib($foreign_da, $cls);
        }
        return $foreign_das;
    }

    /**
     * Process entities so that any incoming reference is depicted in entity itself (i.e. when back_reference allowed).
     * @param Entity $entity
     * @param Cls $cls
     * @param array $mapped_by
     * @param Cls|null $primary_class
     * @return Cls
     */
    private function refineMainClass(Entity $entity, Cls &$cls, array &$mapped_by, Cls $primary_class=null)
    {
        // put in bounds with fetch LAZY
        // @TODO: what if foreign is part of primary
        $used_names = [];
        foreach ($entity->in_bound_fc as $constraint) {
            if($constraint->resident_table->is_link_entity){
                foreach ($constraint->resident_table->getLERecords($constraint) as $record) {

                    $attr = new Attrib();
                    $rt = ForeignConstraint::linkTypeToStr($record->rel_type);
                    $attr->annots[] = Annotation::fromArray($rt, ['fetch' => 'FetchType.LAZY']);

                    $oc = $record->getOther($constraint);
                    $is_many = substr($rt, -strlen('Many')) === 'Many'? true : false;
                    $ref_entity_type = self::tableToClass($oc->foreign_table->name);
                    $possibility = self::columnToAttr($ref_entity_type).'Via'.self::tableToClass($oc->resident_table->name);
                    $attr->name = self::getForeignAttrName($ref_entity_type, $oc->resident_columns,$used_names, $is_many, [$possibility]);
                    $attr->type = $is_many? "List<$ref_entity_type>" : $ref_entity_type;
                    if($is_many) {
                        $attr->default = "new ArrayList<>()";
                        $cls->imports[] = "java.util.List";
                        $cls->imports[] = "java.util.ArrayList";
                    }

                    $join_columns = ['name' => self::q($constraint->resident_table->getName())];
                    foreach (['joinColumns' => $constraint, 'inverseJoinColumns' => $oc] as $mkey => $c) {
                        $join_columns[$mkey] = self::joinColumns($c);
                    }

                    $attr->annots[] = Annotation::fromArray('JoinTable', $join_columns);
                    
                    if($this->config->isJsonIgnore()){
                        $cls->imports[] = 'com.fasterxml.jackson.annotation.JsonIgnore';
                        $attr->annots[] = new Annotation("JsonIgnore");
                    }
                    
                    $cls->attrs[] = $attr;
                }
            } else {
                // Based on type it could be List or Single
                // If not link table
                $ref_entity_type = self::tableToClass($constraint->resident_table->name);
                $attr = new Attrib();
                $rt = $constraint->getLinkTypeString();
                $is_many = substr($rt, -strlen('Many')) === 'Many'? true : false;
                $possibility = count($constraint->resident_columns) < 2 ? self::columnToAttr($ref_entity_type).'Using'.self::tableToClass($constraint->resident_columns[0]->name):[];
                // If self referencing
                // In this case resident column is likely be this entities name, so we use $ref_entity_type
                $attr->name = self::getForeignAttrName($ref_entity_type, [],$used_names, $is_many, [$possibility]);
                $attr->type = $is_many? "List<$ref_entity_type>" : $ref_entity_type; //
                if($is_many) {
                    $attr->default = "new ArrayList<>()";
                    $cls->imports[] = "java.util.List";
                    $cls->imports[] = "java.util.ArrayList";
                }
                $look_up_key = "{$constraint->resident_table->getName()}.{$constraint->constraint_name}";
                $mapped_by_name = $mapped_by[$look_up_key];
                $attr->annots[] = Annotation::fromArray($rt,
                    ['mappedBy' => self::q($mapped_by_name),
                        'fetch' => 'FetchType.LAZY']);
                if($this->config->isJsonIgnore()){
                    $cls->imports[] = 'com.fasterxml.jackson.annotation.JsonIgnore';
                    $attr->annots[] = new Annotation("JsonIgnore");
                }
                $cls->attrs[] = $attr;
            }
        }

        $cls->buildImports();
        return $cls;
    }

    private function normalAttrib($dataAttribute, Cls &$cls)
    {
        $attr = Attrib::buildFromDataAttribute($dataAttribute, self::columnToAttr($dataAttribute->name), $cls, $this->config);
        $this->resolveType($cls, $attr, $dataAttribute);
        $cls->attrs[] = $attr;
    }

    public static function getNomenclature($link_type, $attrib_name)
    {
        $isMany = substr(ForeignConstraint::linkTypeToStr($link_type), -strlen('Many')) === 'Many'? true : false;
        return self::columnToAttr($attrib_name). ($isMany ? 'List': '');
    }

    /**
     * @param string $ref_entity_type
     * @param DataAttribute[] $columns
     * @param array $used_names
     * @param bool $is_many
     * @return string
     */
    public static function getForeignAttrName(string $ref_entity_type, array $columns, array &$used_names=[], bool $is_many=false, array $possibilities=[])
    {
        $synthetic_name = self::columnToAttr($ref_entity_type);
        $altered = !in_array($synthetic_name, $used_names);

        if(!$altered){
            foreach ($possibilities as $possibility) {
                if(!in_array($possibility, $used_names)){
                    $synthetic_name = $possibility;
                    $altered = true;
                    break;
                }
            }
        }

        if(!empty($columns) && count($columns) < 2){
            $col_name = $columns[0]->name;
            $re_list = ['/(\w+)\_id/i', '/id\_(\w+)/i'];

            foreach ($re_list as $re) {
                if(preg_match($re, $col_name) > 0){
                    $expected_name = self::columnToAttr(preg_replace($re, '$1', $col_name));
                    if(!in_array($expected_name, $used_names)){
                        $synthetic_name = $expected_name;
                        $altered = true;
                        break;
                    }
                }
            }

            if(!$altered){
                $synthetic_name = self::columnToAttr($col_name);
            }
        }
        if(in_array($synthetic_name, $used_names)){
            $i = 1;
            while (in_array("{$synthetic_name}$i", $used_names)){
                $i++;
            }
            $synthetic_name = "{$synthetic_name}$i";
        }
        $used_names[] = $synthetic_name;

        return $is_many? "{$synthetic_name}List": $synthetic_name;
    }

    public static function joinColumns(ForeignConstraint $constraint)
    {
        $joins = [];
        $cnt = min(count($constraint->resident_columns), count($constraint->foreign_columns));
        // JoinColumn attribute
        for ($i=0; $i < $cnt; ++$i) {
            $r_name = $constraint->resident_columns[$i]->name;
            $f_name = $constraint->foreign_columns[$i]->name;
            $foreign_table = self::tableToClass($constraint->foreign_table->name);
            $is_unique = $constraint->resident_columns[$i]->is_unique ? "true" : "false";
            $joins[] = Annotation::fromArray('JoinColumn', ["name" => "\"$r_name\"", "referencedColumnName" => "\"$f_name\"",
                "unique" => $is_unique]);
        }
        return $joins;
    }

    private function dumpClass(?Cls $cls)
    {
        if(is_null($cls))
            return;
        $this->dumpAny($cls->getClassFileName(), $this->config->getEntityDir(), "entity_dec", ['cls' => $cls]);

    }

    private function dumpAny($file_name, $dump_dir, $template, array $symbols)
    {
        extract($symbols);
        ob_start();

        include "templates/{$template}.php";

        $content = ob_get_contents();

        ob_end_clean();
        // @todo: each type will have own like entity, repository
        $file_path = implode(DIRECTORY_SEPARATOR, [$this->config->getOutDir(), $dump_dir, "$file_name"]);
        $this->writeContent($content, $file_path);
    }

    private function writeContent($content, $file_path)
    {
        if(!$this->config->isConsole()){
            $dd = dirname($file_path);
            if(!is_dir($dd))
                mkdir($dd, 0777, true);
            file_put_contents($file_path, $content);
        }else{
            echo "\n// Writing to $file_path\n";
            echo "$content\n";
        }
    }


}



class ExternalPlugin implements ILoadablePlugin{

    public $hibernate;

    public function __construct()
    {
        $this->hibernate = new Hibernate();
    }

    public function getName()
    {
        return basename(__FILE__, '.php');
    }

    public function configure($params)
    {
        if(is_null($this->hibernate))
            $this->hibernate = new Hibernate();
        $this->hibernate->configure($params);
    }

    public function run($params)
    {
        return $this->hibernate->template_process($params);
    }

    public function clean($params)
    {
        $this->hibernate = null;
    }

    public function help()
    {
        return "
When command like 
php runner.php -d pg --processor=hibernate --config=/tmp/config.php -- kserver_local

Example config file (config.php):
        
<?php
return [
    'out_dir' => '/tmp/gens',
    'is_console' => false,
    'package' => 'com.test.project',
    'entity_dir' => 'entities',
    'ctrl_dir' => 'controllers',
    'repository_dir' => 'repositories',
    'type_dir' => 'dtos.attributes',
    'back_reference' => true,
    'json_ignore' => true,
    'ignore_relations' => false,
];

        
        ";
    }
}

return __NAMESPACE__;