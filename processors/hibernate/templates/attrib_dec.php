<?php
namespace DBDump\Processor\Hibernate;
/**
 * @global TypeDetail $td
 */
?>

package <? echo "{$td->package}"; ?>;

import java.io.Serializable;

public class <?php echo "{$td->name}"; ?> implements Serializable {
    
    private static final long serialVersionUID = 1L;

}