<?php
namespace DBDump\Processor\Hibernate;
/**
 * @global string $package
 * @global \Entity $entity
 * @global \SchemaPack $schema
 * @global \DBStore $db
 * @global array $imports
 * @global Cls $cls
 */
?>

package <? echo "{$cls->package}"; ?>;

import javax.persistence.*;
import java.io.Serializable;

<?php foreach ($cls->imports as $import) { ?>
import <?php echo "$import;"; ?>

<?php } ?>

// <?php echo $cls->entity->is_link_entity? "Link Entity: {$cls->entity->getName()}": "Main Entity: {$cls->entity->getName()}";?>

<?php

foreach ($cls->annots as $annot){
    echo "$annot\n";
}
?>
public class <?php echo "{$cls->name}"; ?> implements Serializable {
    
    private static final long serialVersionUID = 1L;

<?php
foreach ($cls->attrs as $attr){
    foreach ($attr->annots as $annot){
?>
    <?php echo "$annot\n"; } ?>
    private <?php echo "{$attr->type} {$attr->name}"; echo is_null($attr->default)? "" : " = {$attr->default}"; ?>;

<?php
    }
?>

}