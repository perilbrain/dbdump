<?php
namespace DBDump\Processor\Hibernate;
/**
 * @global string $package
 * @global \Entity $entity
 * @global \SchemaPack $schema
 * @global \DBStore $db
 * @global array $imports
 * @global Repo $cls
 * @global string $main
 * @global string $primary
 */
?>

package <? echo "{$cls->package}"; ?>;

import org.springframework.data.jpa.repository.JpaRepository;

<?php foreach ($cls->imports as $import) { ?>
import <?php echo "$import;"; ?>

<?php } ?>

<?php

foreach ($cls->annots as $annot){
    echo "$annot\n";
}
?>
public interface <?php echo "{$cls->name}"; ?> extends JpaRepository<<?php echo "{$cls->main_cls->name}"; ?>, <?php echo "{$cls->primary_type}"; ?>> {

}