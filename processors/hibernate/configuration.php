<?php
/**
 * Created by PhpStorm.
 * User: kykan
 * Date: 31/1/21
 * Time: 6:16 PM
 */

namespace DBDump\Processor\Hibernate;





trait JsonUpdate{

    public static function tableToClass($str){ return self::camelCase($str, true); }
    public static function columnToAttr($str){ return self::camelCase($str); }
    public static function q($str){ return "\"$str\""; }

    public static function camelCase($str, $first_capital=false, array $no_strip = [])
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $no_strip) . ']+/i', ' ', $str);
        $str = str_replace(" ", "", ucwords(trim($str)));
        // uppercase the first character of each word
        return $first_capital?ucfirst($str):lcfirst($str);
    }

    public function update(array $config)
    {
        foreach ($config as $key => $value) {
            $setter = 'set'.self::camelCase($key, true);
            if(method_exists($this, $setter))
                $this->$setter($value);
            else
                trigger_error(get_class($this)." property $key is not valid", E_USER_WARNING);
        }
    }
}

class TypeMap implements \JsonSerializable {
    use JsonUpdate;

    public $type_name;
    public $use_typedef = true;
    public $typedef = [];
    public $typedef_imports = [];
    public $declared_type;
    public $attribute_type;
    public $attribute_imports = [];

    /**
     * @param array $tm_list
     * @return TypeMap[]
     */
    public static function typeMapFromArray(array $tm_list){
        $built_tm_list = [];
        foreach ($tm_list as $tm) {
            $type_map = new TypeMap();
            $type_map->update($tm);
            $built_tm_list[] = $type_map;
        }
        return $built_tm_list;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->type_name;
    }

    /**
     * @param string $type_name
     * @return TypeMap
     */
    public function setTypeName($type_name)
    {
        $this->type_name = $type_name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseTypedef(): bool
    {
        return $this->use_typedef;
    }

    /**
     * @param bool $use_typedef
     * @return TypeMap
     */
    public function setUseTypedef(bool $use_typedef): TypeMap
    {
        $this->use_typedef = $use_typedef;
        return $this;
    }

    /**
     * @return array
     */
    public function getTypedef(): array
    {
        return $this->typedef;
    }

    /**
     * @param array $typedef
     * @return TypeMap
     */
    public function setTypedef(array $typedef): TypeMap
    {
        $this->typedef = $typedef;
        return $this;
    }

    /**
     * @return array
     */
    public function getTypedefImports(): array
    {
        return array_unique($this->typedef_imports);
    }

    /**
     * @param array $typedef_imports
     * @return TypeMap
     */
    public function setTypedefImports(array $typedef_imports): TypeMap
    {
        $this->typedef_imports = $typedef_imports;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeclaredType()
    {
        return $this->declared_type;
    }

    /**
     * @param string $declared_type
     * @return TypeMap
     */
    public function setDeclaredType($declared_type)
    {
        $this->declared_type = $declared_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttributeType()
    {
        return $this->attribute_type;
    }

    /**
     * @param string $attribute_type
     * @return TypeMap
     */
    public function setAttributeType($attribute_type)
    {
        $this->attribute_type = $attribute_type;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributeImports(): array
    {
        return array_unique($this->attribute_imports);
    }

    /**
     * @param array $attribute_imports
     * @return TypeMap
     */
    public function setAttributeImports(array $attribute_imports): TypeMap
    {
        $this->attribute_imports = $attribute_imports;
        return $this;
    }


}


class Configuration implements \JsonSerializable {
    use JsonUpdate;

    public $is_console = true;
    public $out_dir = "-";
    public $use_embed = false;
    public $use_catalog = false;
    public $back_reference = true;
    public $json_ignore = false;
    public $package = "com.trial.hibernate";
    public $entity_dir = "entities";
    public $type_dir = "dtos.attributes";
    public $ctrl_dir = "controllers";
    public $repository_dir = "repositories";
    public $ignore_unknown_type = false;
    public $ignore_relations = false;
    // public $overwrite = [];
    public $type_maps = [];

    public function updateFromFile(string $config_file)
    {
        if(!is_file($config_file)) {
            trigger_error("config file: $config_file is not valid", E_USER_WARNING);
            return false;
        }
        $config = include("$config_file");
        $this->update($config);
    }

    public function getPackageNameFor($directory_name){
        return "{$this->getPackage()}.$directory_name";
    }

    public function getTypePackage(){
        return $this->getPackageNameFor($this->type_dir);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return bool
     */
    public function isConsole(): bool
    {
        return $this->is_console;
    }

    /**
     * @param bool $is_console
     * @return Configuration
     */
    public function setIsConsole(bool $is_console): Configuration
    {
        $this->is_console = $is_console;
        return $this;
    }

    /**
     * @return string
     */
    public function getOutDir(): string
    {
        return $this->out_dir;
    }

    /**
     * @param string $out_dir
     * @return Configuration
     */
    public function setOutDir(string $out_dir): Configuration
    {
        $this->out_dir = $out_dir;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseEmbed(): bool
    {
        return $this->use_embed;
    }

    /**
     * @param bool $use_embed
     * @return Configuration
     */
    public function setUseEmbed(bool $use_embed): Configuration
    {
        $this->use_embed = $use_embed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseCatalog(): bool
    {
        return $this->use_catalog;
    }

    /**
     * @param bool $use_catalog
     * @return Configuration
     */
    public function setUseCatalog(bool $use_catalog): Configuration
    {
        $this->use_catalog = $use_catalog;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBackReference(): bool
    {
        return $this->back_reference;
    }

    /**
     * @return bool
     */
    public function isActuallyBackReference(): bool
    {
        return (!$this->isIgnoreRelations()) && $this->back_reference;
    }

    /**
     * @param bool $back_reference
     * @return Configuration
     */
    public function setBackReference(bool $back_reference): Configuration
    {
        $this->back_reference = $back_reference;
        return $this;
    }

    /**
     * @return bool
     */
    public function isJsonIgnore(): bool
    {
        return $this->json_ignore;
    }

    /**
     * @param bool $json_ignore
     * @return Configuration
     */
    public function setJsonIgnore(bool $json_ignore): Configuration
    {
        $this->json_ignore = $json_ignore;
        return $this;
    }

    /**
     * @return string
     */
    public function getPackage(): string
    {
        return $this->package;
    }

    /**
     * @param string $package
     * @return Configuration
     */
    public function setPackage(string $package): Configuration
    {
        $this->package = $package;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityDir(): string
    {
        return $this->entity_dir;
    }

    /**
     * @param string $entity_dir
     * @return Configuration
     */
    public function setEntityDir(string $entity_dir): Configuration
    {
        $this->entity_dir = $entity_dir;
        return $this;
    }

    /**
     * @return string
     */
    public function getTypeDir(): string
    {
        return $this->type_dir;
    }

    /**
     * @param string $type_dir
     * @return Configuration
     */
    public function setTypeDir(string $type_dir): Configuration
    {
        $this->type_dir = $type_dir;
        return $this;
    }

    /**
     * @return string
     */
    public function getCtrlDir(): string
    {
        return $this->ctrl_dir;
    }

    /**
     * @param string $ctrl_dir
     * @return Configuration
     */
    public function setCtrlDir(string $ctrl_dir): Configuration
    {
        $this->ctrl_dir = $ctrl_dir;
        return $this;
    }

    /**
     * @return string
     */
    public function getRepositoryDir(): string
    {
        return $this->repository_dir;
    }

    /**
     * @param string $repository_dir
     * @return Configuration
     */
    public function setRepositoryDir(string $repository_dir): Configuration
    {
        $this->repository_dir = $repository_dir;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIgnoreUnknownType(): bool
    {
        return $this->ignore_unknown_type;
    }

    /**
     * @param bool $ignore_unknown_type
     * @return Configuration
     */
    public function setIgnoreUnknownType(bool $ignore_unknown_type): Configuration
    {
        $this->ignore_unknown_type = $ignore_unknown_type;
        return $this;
    }


    /**
     * @return bool
     */
    public function isIgnoreRelations(): bool
    {
        return $this->ignore_relations;
    }

    /**
     * @param bool $ignore_relations
     * @return Configuration
     */
    public function setIgnoreRelations(bool $ignore_relations): Configuration
    {
        $this->ignore_relations = $ignore_relations;
        return $this;
    }

    /**
     * @return array
     */
    public function getTypeMaps(): array
    {
        return $this->type_maps;
    }

    /**
     * @param TypeMap[] $type_maps
     */
    public function mergeTypeMapOptional(array $type_maps){
        foreach ($type_maps as $type_map){
            if(!key_exists($type_map->getTypeName(), $this->type_maps)){
                $this->type_maps[$type_map->getTypeName()] = $type_map;
            }
        }
    }

    /**
     * @param array $type_maps
     * @return Configuration
     */
    public function setTypeMaps(array $type_maps): Configuration
    {
        if(!empty($type_maps)) {
            $tm_list = TypeMap::typeMapFromArray($type_maps);
            foreach ($tm_list as $tm) {
                $this->type_maps[$tm->getTypeName()] = $tm;
            }
        } else {
            $this->type_maps = [];
        }
        return $this;
    }

    /**
     * @param string $key
     * @return TypeMap
     */
    public function getTypeMapFor(string $key)
    {
        if(!key_exists($key, $this->type_maps))
            return null;
        return $this->type_maps[$key];
    }


}
