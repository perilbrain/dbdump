<?php
namespace DBDump\Processor\Hibernate;
require_once 'core/core.php';
require_once 'configuration.php';

use ForeignConstraint;
use Constraint;
use DataAttribute;
use Entity;
use SchemaPack;
use DBStore;

class KV{
    public $key;
    public $value;

    /**
     * KV constructor.
     * @param string $key
     * @param string $value
     */
    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public static function build($key, $value)
    {
        return new KV($key, $value);
    }

    public function __toString()
    {
        $new_val = $this->value;
        if(is_array($new_val)){
            $new_val = '{ '.implode(', ', $new_val).' }';
        }
        return "{$this->key}={$new_val}";
    }
}

class Annotation{
    public $name;
    public $kv_arr = [];
    public $sub_annots = [];

    /**
     * Annotation constructor.
     * @param string $name
     * @param array $kv_arr
     */
    public function __construct($name, $kv_arr=[])
    {
        $this->name = $name;
        $this->kv_arr = $kv_arr;
    }

    /**
     * @param string $name
     * @param array $kv_arr
     * @return Annotation
     */
    public static function fromArray($name, array $kv_arr)
    {
        $annot = new Annotation("$name");
        foreach ($kv_arr as $key => $value) {
            $annot->kv_arr[] = is_int($key)? $value : new KV($key, $value);
        }
        return $annot;
    }

    public function __toString()
    {
        $kv_arr = $this->kv_arr;
        if(!empty($this->sub_annots)) {
            $joined = '{ ' . implode(', ', $this->sub_annots) . ' }';
            $kv_arr[] = $joined;
        }
        return "@{$this->name}".(empty($kv_arr)? "" : "(".implode(", ", $kv_arr).")");
    }
}

class TypeDetail{
    public $name;
    public $package;
    public $is_array;

    public function getClassFileName() { return "{$this->name}.java"; }

    public function __toString() {
        return $this->package.$this->name;
    }
}

class Attrib{
    public $name;
    /**
     * @var Annotation[] $annots
     */
    public $annots = [];
    public $imports = [];
    public $type;
    /**
     * @var DataAttribute $da
     */
    public $da;
    public $default = null;
    /**
     * @var TypeDetail $type_detail
     */
    public $type_detail = null;

    public static function buildFromDataAttribute(DataAttribute $dataAttribute, $name, Cls $cls, Configuration $config)
    {
        $annotate = $cls->is_entity || ($config->use_embed && !$cls->is_entity);
        $attrib = new Attrib();
        $attrib->name = $name;
        $attrib->da = $dataAttribute;
        // annots pending
        if($annotate){

            if($dataAttribute->is_primary || (!$config->use_embed && in_array($dataAttribute, $dataAttribute->getParent()->primary_keys))) {
                $attrib->annots[] = new Annotation("Id");
                if(!is_null($dataAttribute->sequence)){
                    $attrib->annots[] = Annotation::fromArray("SequenceGenerator", [
                        'name' => "\"$dataAttribute->sequence\"",
                        'sequenceName' => "\"$dataAttribute->sequence\"",
                        'allocationSize' => 1,
                    ]);
                    $attrib->annots[] = Annotation::fromArray("GeneratedValue", [
                        'generator' => "\"$dataAttribute->sequence\"",
                        'strategy' => "GenerationType.SEQUENCE"
                    ]);
                }
                if(!is_null($dataAttribute->identity)){
                    // may be @org.hibernate.annotations.Generated(value = GenerationTime.INSERT)
                    $attrib->annots[] = Annotation::fromArray("GeneratedValue", [
                        'strategy' => "GenerationType.IDENTITY"
                    ]);
                }
            }


            $annot = Annotation::fromArray("Column", [
                'name' => "\"{$dataAttribute->name}\""
                // ,'table' => "\"{$dataAttribute->getParent()->getName()}\""
            ]);
            if($dataAttribute->is_unique)
                $annot->kv_arr[] = KV::build("unique","true");
            $annot->kv_arr[] = KV::build("nullable",$dataAttribute->is_nullable?"true":"false");
            $annot->kv_arr[] = KV::build("updatable",$dataAttribute->is_updatable?"true":"false");
            if(!empty($dataAttribute->numeric_precision))
                $annot->kv_arr[] = KV::build("precision","{$dataAttribute->numeric_precision}");
            if(!empty($dataAttribute->numeric_precision_radix))
                $annot->kv_arr[] = KV::build("scale","{$dataAttribute->numeric_precision_radix}");
            if(!empty($dataAttribute->character_maximum_length))
                $annot->kv_arr[] = KV::build("length","{$dataAttribute->character_maximum_length}");

            $attrib->annots[] = $annot;
        }
        return $attrib;
    }

    public function addToAnnotation(string $annotationName, array $kv_arr=[], array $sub_annots=[])
    {
        foreach(array_filter($this->annots, function (Annotation $a) use($annotationName){ return$a->name === $annotationName;}) as &$annotation){
            $annotation->kv_arr = array_merge($annotation->kv_arr, $kv_arr);
            $annotation->sub_annots = array_merge($annotation->sub_annots, $sub_annots);
        }
    }

    public function extractTypeFromComment($type_package="")
    {
        $comment = $this->da->comment;
        $re = '/Type\((?:([#a-zA-Z_$][a-zA-Z\d_$]*(?:\.[a-zA-Z_$][a-zA-Z\d_$]*)*)\.)?([a-zA-Z_$][a-zA-Z\d_$]*)\)(\[\])?/m';
        $activation_flag = '#T';
        if(!is_null($comment)){
            preg_match_all($re, $comment, $matches);
            if(count($matches) < 3 || empty($matches[2]))
                return null;
            $pkg = $matches[1][0];
            $cls_name = Configuration::tableToClass($matches[2][0]);
            $is_array = isset($matches[3]) && isset($matches[3][0]) && $matches[3][0] == '[]';
            if(!empty($pkg) && strpos($pkg, $activation_flag) !== false){
                $pkg = str_replace($activation_flag, $type_package, $pkg);
                $this->imports[] = "$pkg.$cls_name";
            }
            $type_detail = new TypeDetail();
            $type_detail->name = $cls_name;
            $type_detail->package = $pkg;
            $type_detail->is_array = $is_array;
            $this->type_detail = $type_detail;
            return $type_detail;
        }
        return null;
    }

    /**
     * @return bool
     */
    public function hasTypeDetail()
    {
        return !is_null($this->type_detail);
    }
}

class Cls{
    public $name;
    /**
     * @var Annotation[] $annots
     */
    public $annots = [];
    public $imports = [];
    /**
     * @var Attrib[] $attrs
     */
    public $attrs = [];
    /**
     * @var Entity $entity
     */
    public $entity;
    public $schema_name;
    public $db_name;
    public $package;
    public $full_package;
    public $id_class = null;
    public $is_entity = true;
    /**
     * @var Attrib|Cls $id_attr
     */
    public $id_attr = null;

    public static function buildFromEntity(Entity $entity, $name, $package)
    {
        $cls = new Cls();
        $cls->setNamePackage($name, $package);
        $schema = $entity->getParent();
        $db = $schema->getParent();
        if($db->isDecoy()){
            $cls->db_name = $schema->getName();
            $cls->schema_name = null;
        }else{
            $cls->db_name = $db->getName();
            $cls->schema_name = $schema->getName();
        }
        $cls->entity = $entity;
        $cls->annots[] = new Annotation("Data");
        $cls->imports[] = "lombok.Data";
        return $cls;
    }

    public function buildImports() {
        $child_imports = empty($this->attrs)? [] : call_user_func_array('array_merge', array_column($this->attrs, 'imports'));
        $this->imports = array_unique(array_merge($this->imports, $child_imports));
        usort($this->imports, function ($a, $b){ return strcmp($a, $b);});
    }

    public function copyAttributes(Cls $cls, bool $copy_id) {
        $this->attrs = array_merge($cls->attrs, $this->attrs);
        $this->imports = array_unique(array_merge($cls->imports, $this->imports));
        if($copy_id)
            $this->id_attr = $cls->id_attr;
    }

    public function hasAnnotation(string $annotationName) {
        return in_array($annotationName, array_column($this->annots, 'name'));
    }

    public function addToAnnotationKey(string $annotationName, $key, array $value)
    {
        foreach(array_filter($this->annots, function (Annotation $a) use($annotationName){ return$a->name === $annotationName;}) as &$annotation){
            foreach (array_filter($annotation->kv_arr, function (KV $kv) use($key){ return $kv->key === $key;}) as &$kv){
                $built_values = is_array($kv->value) ? $kv->value : [$kv->value];
                $kv->value = array_unique(array_merge($built_values, $value));
            }
        }
    }

    public function addOptionallyToArrayAnnotation(string $array_annotation_name, $key, Annotation $annotation, array $imports=[]){
        if(!$this->hasAnnotation($array_annotation_name)){
            $this->annots[] = Annotation::fromArray($array_annotation_name, [$key => []]);
        }
        $this->imports= array_unique(array_merge($this->imports, $imports));
        $this->addToAnnotationKey($array_annotation_name,$key, [$annotation]);
    }

    public function getClassFileName() { return "{$this->name}.java"; }

    public function setNamePackage($name, $package) {
        $this->name = $name;
        $this->package = $package;
        $this->full_package = "$package.$name";
    }

    public function getAllTypeDetails()
    {
        $type_details = [];
        foreach ($this->attrs as $attrib){
            if($attrib->hasTypeDetail())
                $type_details[] = $attrib->type_detail;
        }
        return $type_details;
    }
}

class Repo extends Cls{
    public $main_cls;
    public $primary_type;

    public static function buildFromCls(Cls $cls, ?Cls $primary_class, $package, $suffix='Repo')
    {
        $repo = new Repo();
        $repo->setNamePackage("{$cls->name}$suffix", $package);
        $repo->main_cls = $cls;
        $repo->imports[] = $cls->full_package;
        if(!is_null($primary_class)){
            $repo->primary_type = $primary_class->name ;
            $repo->imports[] = $primary_class->full_package;
        }else{
            if(!is_null($cls->id_attr))
                $repo->primary_type = $cls->id_attr->type ;
        }
        return $repo;
    }
}

