<?php
namespace DBDump\Processor\Hibernate;

require_once 'configuration.php';
require_once 'entity_gen.php';
use DataAttribute;

function throwUnKnownOrGet(Configuration $configuration, $dtype, DataAttribute $da){
    $table_name = $da->getParent()->tb_name;
    $column_name = $da->name;
    $tm = $configuration->getTypeMapFor($dtype);
    if(is_null($tm)){
        throw new \RuntimeException("Don't know how to process $dtype for Table: $table_name, Column: $column_name, have you defined type_maps ??");
    }
    return $tm;
}

function setTypeAnnotation(Attrib &$attrib, $declared_type){
    if(!is_null($declared_type)){
        $attrib->annots[] = Annotation::fromArray("Type", ["type" => "\"$declared_type\""]);
        $attrib->imports[] = "org.hibernate.annotations.Type";
    }
}

function processJson(Attrib &$attrib, Cls &$cls, Configuration &$configuration, $pg_type, TypeMap $tm, $java_type='JsonBinaryType'){

    setTypeAnnotation($attrib, $tm->getDeclaredType());

    $type_detail = $attrib->extractTypeFromComment($configuration->getTypePackage());
    $attrib->default = null;
    if(is_null($type_detail)) {
        $attrib->imports = array_merge($attrib->imports, $tm->getAttributeImports());
        $attrib->type = $tm->getAttributeType();
    }else{
        $attrib->type = "{$type_detail->name}";
        if($type_detail->is_array){
            $attrib->default = "new ArrayList<{$type_detail->name}>()";
            $attrib->type = "List<{$type_detail->name}>";
            $cls->imports[] = "java.util.List";
            $cls->imports[] = "java.util.ArrayList";
        }
    }
    $attrib->addToAnnotation('Column', [KV::build('columnDefinition', "\"$pg_type\"")]);
    $typedef_annotation = Annotation::fromArray('TypeDef', $tm->getTypedef());
    $cls->addOptionallyToArrayAnnotation('TypeDefs','value', $typedef_annotation, $tm->getTypedefImports());
}


function pg_process_attribute(Cls &$cls, Attrib &$attrib, DataAttribute $da, Configuration $config){
    $dtype = strtolower($da->dtype);
    $timestampCheck = function () use (&$attrib){
        $a_name = str_replace("_", "", strtolower($attrib->name));
        if(in_array($a_name, ['updatedat', 'updated', 'lastupdated'])){
            $attrib->annots[] =  new Annotation("UpdateTimestamp");
            $attrib->imports[] = "org.hibernate.annotations.UpdateTimestamp";
        }
        if(in_array($a_name, ['createdat', 'created', 'insertedat'])){
            $attrib->annots[] =  new Annotation("CreationTimestamp");
            $attrib->imports[] = "org.hibernate.annotations.CreationTimestamp";
        }
    };
    $basic = new Annotation("Basic");
    $attrib->default = !is_null($da->def_val) && (!empty($da->def_val) || $da->def_val==0)
    && !$da->is_primary && is_null($da->sequence) ? $da->def_val : null;

    $table_name = $da->getParent()->tb_name;
    $column_name = $da->name;

    switch ($dtype){
        case 'bigint':
            $attrib->type = "Long";
            if(!is_null($attrib->default))
                $attrib->default ="{$attrib->default}L";
            break;
        case 'boolean':
            $attrib->type = "Boolean";
            break;
        case 'integer':
            $attrib->type = "Integer";
            break;
        case 'smallint':
            $attrib->type = "Short";
            break;
        case 'real':
            $attrib->type = "Float";
            break;
        case 'double precision':
            $attrib->type = "Double";
            break;
        case 'numeric':
        case 'decimal':
            $attrib->imports[] = "java.math.BigDecimal";
            $attrib->type = "BigDecimal";
            break;
        case 'uuid':
            $attrib->imports[] = "java.util.UUID";
            $attrib->type = "UUID";
            $attrib->default = null;
            break;
        case 'character':
            $attrib->addToAnnotation('Column', [KV::build('columnDefinition', '"bpchar"')]);
        case 'character varying':
        case 'text':
            array_unshift($attrib->annots, $basic);
            $attrib->type = "String";
            if(!is_null($attrib->default) && strpos($attrib->default, '::') > 0){
                $attrib->default = '"'. substr(explode('::', str_replace("''", "'", $attrib->default) )[0], 1, -1).'"';
            }

            break;
        case 'inet':
            $tm = throwUnKnownOrGet($config, $dtype, $da);
            $attrib->imports = array_merge($attrib->imports, $tm->getAttributeImports());
            $attrib->type = $tm->getAttributeType();
            $attrib->addToAnnotation('Column', [KV::build('columnDefinition', "\"{$tm->getTypeName()}\"")]);
            $attrib->default = null;
            $typedef_annot = Annotation::fromArray('TypeDef', $tm->getTypedef());
            $cls->addOptionallyToArrayAnnotation('TypeDefs','value', $typedef_annot, $tm->getTypedefImports());
            setTypeAnnotation($attrib, $tm->getDeclaredType());
            break;
        case 'json':
            $tm = throwUnKnownOrGet($config, $dtype, $da);
            array_unshift($attrib->annots, $basic);
            processJson($attrib, $cls, $config, 'json', $tm, 'JsonStringType');
            break;
        case 'jsonb':
            $tm = throwUnKnownOrGet($config, $dtype, $da);
            array_unshift($attrib->annots, $basic);
            processJson($attrib, $cls, $config, 'jsonb', $tm, 'JsonBinaryType');
            break;
        case 'date':
            $attrib->imports[] = "java.sql.Date";
            $attrib->type = "Date";
            $attrib->default = null;
            break;
        case 'time without time zone':
        case 'time with time zone':
            $timestampCheck();
            $attrib->imports[] = "java.sql.Time";
            $attrib->type = "Time";
            $attrib->default = null;
            break;
        case 'timestamp without time zone':
        case 'timestamp with time zone':
            $timestampCheck();
            $attrib->imports[] = "java.sql.Timestamp";
            $attrib->type = "Timestamp";
            $attrib->default = null;
            break;
        default:
            if($config->isIgnoreUnknownType()){
            } else {
                throw new \RuntimeException("Unknown type: $dtype for Table: $table_name, Column: $column_name");
                break;
            }
    }

    if($da->isFormula() && !is_null($da->generation_expression)){
        $attrib->annots[] =  Annotation::fromArray("Formula", ['value' => "\"{$da->generation_expression}\""]);
        $attrib->imports[] = "org.hibernate.annotations.Formula";
    }
}


function pg_setup_typeMap(Configuration &$configuration){
    $type_maps = [
        [
            'type_name' => 'json',
            'use_typedef' => true,
            // 'typedef' => ['name' => '"json"', 'typeClass' => "JsonStringType.class"],
            'typedef' => ['name' => '"jsonb"', 'typeClass' => "JsonBinaryType.class"],
            'typedef_imports' => ["org.hibernate.annotations.TypeDef","org.hibernate.annotations.TypeDefs",
                // "com.vladmihalcea.hibernate.type.json.JsonStringType",
                "com.vladmihalcea.hibernate.type.json.JsonBinaryType"],
            'declared_type' => 'jsonb',
            'attribute_type' => 'JsonNode',
            'attribute_imports' => ['com.fasterxml.jackson.databind.JsonNode']
        ],
        [
            'type_name' => 'jsonb',
            'use_typedef' => true,
            'typedef' => ['name' => '"jsonb"', 'typeClass' => "JsonBinaryType.class"],
            'typedef_imports' => ["org.hibernate.annotations.TypeDef","org.hibernate.annotations.TypeDefs",
                "com.vladmihalcea.hibernate.type.json.JsonBinaryType"],
            'declared_type' => 'jsonb',
            'attribute_type' => 'JsonNode',
            'attribute_imports' => ['com.fasterxml.jackson.databind.JsonNode']
        ],
        [
            'type_name' => 'inet',
            'use_typedef' => true,
            'typedef' => ['name' => '"ipv4"', 'typeClass' => 'PostgreSQLInetType.class', 'defaultForType' => 'Inet.class'],
            'typedef_imports' => ["org.hibernate.annotations.TypeDef","org.hibernate.annotations.TypeDefs",
                "com.vladmihalcea.hibernate.type.basic.PostgreSQLInetType"],
            'declared_type' => null,
            'attribute_type' => 'Inet',
            'attribute_imports' => ['com.vladmihalcea.hibernate.type.basic.Inet']
        ],
    ];

    $configuration->mergeTypeMapOptional(TypeMap::typeMapFromArray($type_maps));

}
