<?php
/**
 * Author: perilbrain
 */

class DBUtil
{
    public static function getCombinations($plain_array, $is_product=false)
    {
        $al = count($plain_array);
        $combos = [];
        for ($i = 0; $i < $al; ++$i) {
            for ($j = $is_product ? 0 : $i + 1; $j < $al; $j++) {
                $combos[] = [$plain_array[$i], $plain_array[$j]];
            }
        }
        return $combos;
    }

    /**
     * @param string $hay_stack
     * @param string $needle
     * @return bool
     */
    public static function hasString(string $hay_stack, string $needle)
    {
        return strpos($hay_stack, $needle) !== false;
    }
}
