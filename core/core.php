<?

require_once 'db_util.php';

interface ChildNature{
    public function getFullName();
    public function getParent();
    public function process();
}
/**
 *
 */
class DataAttribute implements \JsonSerializable, ChildNature {
	public $name;
	public $def_val = null;
	public $dtype;
	public $comment = null;
    /**
     * @var LinkAttribute[]
     */
	public $is_nullable = false;
	public $is_updatable = true;
	public $character_maximum_length;
	public $numeric_precision;
	public $numeric_precision_radix;
	public $sequence = null;
	public $identity = null;
	public $is_generated;
	public $generation_expression = null;
    /**
     * @var Entity $entity
     */
	public $entity;

	public $is_primary = false;
	public $is_unique = false;
	public $is_foreign = false;
    /**
     * @var ForeignConstraint $foreign_constraint
     */
	public $foreign_constraint = null;

	function __construct() {

	}

	public static function create($name, $dtype, $def_val = null) {
		$instance = new self();
		$instance->name = $name;
		$instance->dtype = $dtype;
		$instance->def_val = $def_val;
		return $instance;
	}

	public function isUnique() {
		return $this->is_primary || $this->is_unique;
	}

	public function isData() {
		return !$this->is_foreign;
	}

	public function isLink() {
		return $this->is_foreign && !is_null($this->foreign_constraint);
	}

	public function isFormula(){
	    return $this->is_generated === 'ALWAYS';
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }


    /**
     * @return Entity
     */
    public function getParent()
    {
        return $this->entity;
    }

    public function process()
    {
        return $this;
    }

    public function getFullName()
    {
        return "{$this->getParent()->getFullName()}.{$this->name}";
    }

    public function __toString() {
        return $this->getFullName();
    }
}



class LinkEntityRecord {
    /**
     * @var ForeignConstraint $left_fc
     */
    public $left_fc;
    /**
     * @var ForeignConstraint $right_fc
     */
    public $right_fc;
    public $rel_type;
    /**
     * @var Entity $parent
     */
    public $parent;

    /**
     * LinkEntityRecord constructor.
     * @param ForeignConstraint $left_fc
     * @param ForeignConstraint $right_fc
     * @param Entity $parent
     */
    public function __construct($left_fc, $right_fc, $parent)
    {
        $this->left_fc = $left_fc;
        $this->right_fc = $right_fc;
        $this->parent = $parent;
        // Evaluate rel
        $this->rel_type = ForeignConstraint::relationCombine($left_fc->link_type, $right_fc->link_type,
            ForeignConstraint::ManyToMany);

    }

    /**
     * @param ForeignConstraint $constraint
     * @return boolean
     */
    public function isParticipating($constraint){
        return $this->right_fc === $constraint || $this->left_fc === $constraint;
    }

    /**
     * @param ForeignConstraint $constraint
     * @return ForeignConstraint
     */
    public function getOther($constraint){
        if($this->right_fc === $constraint)
            return $this->left_fc;
        if($this->left_fc === $constraint)
            return $this->right_fc;
        return null;
    }

}

/**
 *
 */
class Entity  implements \JsonSerializable, ChildNature {
	public $name;
	public $tb_name;

	public $table_comment = null;
    /**
     * @var DataAttribute[]
     */
	public $data_attributes = [];
	public $primary_keys = [];

    /**
     * @var ForeignConstraint[] Associative
     */
	public $foreign_constraints = [];

    /**
     * @var Constraint[] Associative
     */
	public $unique_constraints = [];

    /**
     * @var Constraint $primary_constraint
     */
	public $primary_constraint = null;
	public $is_link_entity = false;

    /**
     * @var ForeignConstraint[] Associative
     */
	public $in_bound_fc = [];
    /**
     * @var LinkEntityRecord[] $link_entity_records
     */
	public $link_entity_records = [];
    /**
     * @var Constraint[] Associative
     */
    public $constraints = [];

    /**
     * @var SchemaPack $schema_pack
     */
	public $schema_pack;
    /**
     * @var Entity $parent_entity
     */
	public $parent_entity = null;
    /**
     * @var Entity[] $child_entities
     */
	public $child_entities = [];

	function __construct($name, $tb_name) {
		$this->name = $name;
		$this->tb_name = $tb_name;
	}

    /**
     * @param DataAttribute $attr_obj
     */
    public function addAttr($attr_obj) {
		$this->data_attributes[$attr_obj->name] = $attr_obj;
		if($attr_obj->is_primary)
		    $this->setPrimary([$attr_obj]);
	}

	public function hasAttr($attr_name) {
		return array_key_exists($attr_name, $this->data_attributes);
	}

	public function attemptAdd($attr_obj) {
		if (!$this->hasAttr($attr_obj->name)) {
			$this->addAttr($attr_obj);
			return true;
		}
		return false;
	}

    /**
     * @return  DataAttribute[]
     */
	public function getDataAttributes(){
	    return array_values($this->data_attributes);
    }

    /**
     * @return  ForeignConstraint[]
     */
    public function nonParentFC() {
        return array_filter($this->foreign_constraints, function (ForeignConstraint $fc) {
            return $fc->resident_table !== $this->parent_entity;
        });
    }

    /**
     * @param bool $prime
     * @param bool $unique
     * @param bool $foreign
     * @return  DataAttribute[]
     */
    public function getKeyAttributes($prime=true, $unique=true, $foreign=true)
    {
        $key_attributes = [];
        foreach ($this->constraints as $constraint) {
            if((!$prime && $constraint->constraint_type === Constraint::PRIMARY)
                || (!$unique && $constraint->constraint_type === Constraint::UNIQUE)
                || (!$foreign && $constraint->constraint_type === Constraint::FOREIGN)
                || !Constraint::isBasicType($constraint->constraint_type))
                continue;
            $key_attributes = array_merge($constraint->resident_columns, $key_attributes);
        }
        return array_unique($key_attributes);
    }

    /**
     * @param bool $discard_prime
     * @param bool $discard_unique
     * @param bool $discard_foreign
     * @return  DataAttribute[]
     */
    public function getNonKeyAttributes($discard_prime=true, $discard_unique=true, $discard_foreign=true) {
        return array_values(array_diff($this->getDataAttributes(), $this->getKeyAttributes($discard_prime, $discard_unique, $discard_foreign)));
    }

	public function isDerived() {
		return !is_null($this->parent_entity);
	}

	public function seemsLink() {
		return $this->is_link_entity;
	}

    public function notALinkAsPerComment()
    {
        if(empty($this->table_comment))
            return false;
        return DBUtil::hasString($this->table_comment, "/LINK:NO");
	}

	public function hasInLinks() {
        return !empty($this->in_bound_fc);
	}

	public function isParent() {
        return !empty($this->child_entities);
	}

    /**
     * @param ForeignConstraint $constraint
     * @return LinkEntityRecord[]
     */
    public function getLERecords($constraint){
        return array_filter($this->link_entity_records, function (LinkEntityRecord $le) use ($constraint){ return $le->isParticipating($constraint);});
    }

    /**
     * @param DataAttribute[] $attr_set
     */
    public function setPrimary($attr_set) {
        if(!is_array($attr_set))
            $attr_set = [$attr_set];
        $this->primary_keys = $attr_set;
        if(count($attr_set) == 1 && !$attr_set[0]->is_primary)
            $attr_set[0]->is_primary = true;
	}

    /**
     * @param Constraint $constraint
     */
    public function setPrimaryConstraint($constraint) {
        if(array_key_exists($constraint->constraint_name, $this->constraints))
            return;
        $this->primary_constraint = $constraint;
        $this->setPrimary($constraint->resident_columns);
        $this->constraints[$constraint->constraint_name] = $constraint;
	}

    /**
     * @param Constraint $constraint
     */
    public function addUnique($constraint) {
        if(array_key_exists($constraint->constraint_name, $this->constraints))
            return;
        if(!$constraint->isComposite())
            $constraint->resident_columns[0]->is_unique = true;
        $this->unique_constraints[$constraint->constraint_name] = $constraint;
        $this->constraints[$constraint->constraint_name] = $constraint;
	}

    /**
     * @param ForeignConstraint $constraint
     */
    public function addForeign($constraint) {
        if(array_key_exists($constraint->constraint_name, $this->constraints))
            return;
        $this->foreign_constraints[$constraint->constraint_name] = $constraint;
        $constraint->foreign_table->addInBoundForeign($constraint);
        if(!$constraint->isComposite()) {
            $constraint->resident_columns[0]->is_foreign = true;
            $constraint->resident_columns[0]->foreign_constraint = $constraint;
        }
        $this->constraints[$constraint->constraint_name] = $constraint;
	}

    /**
     * @param ForeignConstraint $constraint
     */
    public function addInBoundForeign($constraint) {
        if(array_key_exists($constraint->constraint_name, $this->in_bound_fc))
            return;
        $this->in_bound_fc[$constraint->constraint_name] = $constraint;
	}

    /**
     * @param Constraint $constraint
     */
    public function addConstraint($constraint) {
        if(array_key_exists($constraint->constraint_name, $this->constraints))
            return;
        $this->constraints[$constraint->constraint_name] = $constraint;
	}

    /**
     * @param DataAttribute[] $data_attributes
     * @param $except_name
     * @param array $valid_types
     * @return Constraint[]
     */
    public function getConstraintsFor($data_attributes, $except_name, $valid_types=[])
    {
        $others = [];
        foreach ($this->constraints as $key => $constraint) {
            if($key == $except_name
                || ( !empty($valid_types) && !in_array($constraint->constraint_type, $valid_types) )
            )
                continue;

            if($constraint->isForAttributes($data_attributes))
                $others[] = $constraint;
        }
        return $others;
	}

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return SchemaPack
     */
    public function getParent()
    {
        return $this->schema_pack;
    }

    public function getName()
    {
        return $this->tb_name;
    }

    public function getFullName()
    {
        return "{$this->getParent()->getFullName()}.{$this->name}";
    }

    public function getLinkCombo()
    {
        $input_array = array_values($this->foreign_constraints);
        return DBUtil::getCombinations($input_array);
    }

    public function process()
    {
        foreach ($this->getDataAttributes() as $attribute) {
            $attribute->process();
        }
        if(count($this->nonParentFC()) == 2 && empty($this->in_bound_fc) && !$this->notALinkAsPerComment()){
            $this->is_link_entity = true;
        }
        if($this->seemsLink()){
            $this->link_entity_records = array_map(function ($e_arr) {
                return new LinkEntityRecord($e_arr[0], $e_arr[1], $this);
                }, $this->getLinkCombo());
        }
        return $this;
    }
}

// This could be a schema or a db, depends on database
class SchemaPack implements \JsonSerializable, ChildNature {
    /**
     * @var boolean
     */
    protected $is_schema;

    /**
     * @var Entity[]
     */
    public $entities = [];

    /**
     * @var string
     */
    protected $name;

    // Use string identity
    // To get instance call getParent(context);
    public $db_store;

    /**
     * SchemaPack constructor.
     * @param bool $is_schema
     * @param string $name
     */
    public function __construct(bool $is_schema, string $name)
    {
        $this->is_schema = $is_schema;
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isSchema(): bool
    {
        return $this->is_schema;
    }

    /**
     * @param bool $is_schema
     */
    public function setIsSchema(bool $is_schema): void
    {
        $this->is_schema = $is_schema;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return DBStore
     */
    public function getParent()
    {
        return $this->db_store;
    }

    public function process()
    {
        foreach ($this->entities as $entity) {
            $entity->process();
        }
        return $this;
    }

    public function getDataAttributes(){
        $da = [];
        foreach ($this->entities as $entity) {
            $da = array_merge($da, $entity->getDataAttributes());
        }
        return $da;
    }

    public function getFullName()
    {
        return $this->getName();
    }
}



class Constraint implements \JsonSerializable{

    const PRIMARY = 1;
    const UNIQUE = 2;
    const FOREIGN = 3;

    public $constraint_name;
    public $resident_schema;
    /**
     * @var Entity $resident_table
     */
    public $resident_table;
    /**
     * @var DataAttribute[] $resident_columns plain array
     */
    public $resident_columns;
    public $constraint_type;
    /**
     * @var Constraint[] $overlapping_cs
     */
    public $overlapping_cs = [];

    public $is_synthetic = false;


    /**
     * Constraint constructor.
     * @param string $constraint_name
     * @param SchemaPack $resident_schema
     * @param Entity $resident_table
     * @param DataAttribute[] $resident_columns
     * @param integer $constraint_type
     */
    public function __construct($constraint_name, $resident_schema, $resident_table, $resident_columns, $constraint_type)
    {
        $this->constraint_name = $constraint_name;
        $this->resident_schema = $resident_schema;
        $this->resident_table = $resident_table;
        $this->resident_columns = $resident_columns;
        $this->constraint_type = $constraint_type;
    }

    public function isComposite()
    {
       return is_array($this->resident_columns) && count($this->resident_columns) > 1;
    }

    public function isUnique()
    {
       return in_array($this->constraint_type, [self::PRIMARY, self::UNIQUE]);
    }

    public function isLink() {
        return false;
    }

    public function getResidentNames($is_full=false)
    {
        return array_map(function (DataAttribute $da) use ($is_full) {
                return $is_full ? $da->getFullName() : $da->name;
            }, $this->resident_columns);
    }

    public function process(){
        $this->setOverlappingOnSame();
    }

    /**
     * @param DataAttribute[] $data_attributes
     * @return boolean
     */
    public function isForAttributes($data_attributes)
    {
        if(!is_array($data_attributes))
            $data_attributes = [$data_attributes];
        $own_set = $this->resident_columns;
        sort($data_attributes);
        sort($own_set);
        return $own_set === $data_attributes;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public static function getBasicTypes(){
        return [self::PRIMARY, self::UNIQUE, self::FOREIGN];
    }

    public static function isBasicType($constraint_type){
        return in_array($constraint_type, self::getBasicTypes());
    }

    /**
     * @return Constraint[]
     */
    private function getOverlappingOnSame()
    {
        $others = [];
        $data_attributes = $this->resident_columns;
        foreach ($this->resident_table->constraints as $key => $constraint) {
            if ($key == $this->constraint_name
                    || !Constraint::isBasicType($constraint->constraint_type)
                )
                    continue;
            $foreign_cols = $constraint->resident_columns;
            sort($data_attributes);
            sort($foreign_cols);

            if($data_attributes === $foreign_cols)
                $others[] = $constraint;
        }
        return $others;
    }

    private function setOverlappingOnSame()
    {
        foreach ($this->getOverlappingOnSame() as $constraint) {
            $this->overlapping_cs[$constraint->constraint_name] = $constraint;
        }
    }
}



class ForeignConstraint extends Constraint implements \JsonSerializable{
    const OneToOne = '1:1';
    const OneToMany = '1:m';
    const ManyToOne = 'm:1';
    const ManyToMany = 'm:n';
    const InverseMap = [self::OneToOne => self::OneToOne, self::OneToMany => self::ManyToOne,
        self::ManyToOne => self::OneToMany, self::ManyToMany => self::ManyToMany];
    const StringMap = [self::OneToOne => 'OneToOne', self::OneToMany => 'ManyToOne',
        self::ManyToOne => 'OneToMany', self::ManyToMany => 'ManyToMany'];

    public $foreign_schema;
    public $foreign_table;
    /**
     * @var DataAttribute[] $foreign_columns
     */
    public $foreign_columns;
    public $link_type = null;
    public $self_referencing = false;

    /**
     * ForeignConstraint constructor.
     * @param string $constraint_name
     * @param SchemaPack $resident_schema
     * @param Entity $resident_table
     * @param DataAttribute[] $resident_columns
     * @param integer $constraint_type
     * @param SchemaPack $foreign_schema
     * @param Entity $foreign_table
     * @param DataAttribute[] $foreign_columns
     */
    public function __construct($constraint_name, $resident_schema, $resident_table, $resident_columns,
                                $constraint_type, $foreign_schema, $foreign_table, $foreign_columns)
    {
        parent::__construct($constraint_name, $resident_schema, $resident_table, $resident_columns, $constraint_type);
        $this->foreign_schema = $foreign_schema;
        $this->foreign_table = $foreign_table;
        $this->foreign_columns = $foreign_columns;
        $this->self_referencing = $resident_table === $foreign_table;
    }

    public function process()
    {
        parent::process();
        // Can't refer overlapping_cs because of race condition.
        $foreign_constraints = $this->foreign_table->getConstraintsFor($this->foreign_columns, $this->constraint_name);
        $res_unique = empty($this->overlapping_cs) ?
            false : count(array_filter($this->overlapping_cs, function (Constraint $c){ return $c->isUnique(); })) > 0;
        $for_unique = empty($foreign_constraints) ?
            false : count(array_filter($foreign_constraints, function (Constraint $c){ return $c->isUnique(); })) > 0;

        if($for_unique && $res_unique){
            $this->link_type = self::OneToOne;
            return $this;
        }
        if($for_unique && !$res_unique){
            $this->link_type = self::ManyToOne;
            return $this;
        }
        if(!$for_unique && $res_unique){
            $this->link_type = self::OneToMany;
            return $this;
        }
        if(!$for_unique && !$res_unique){
            $this->link_type = self::ManyToMany;
            return $this;
        }
    }

    public function isLink() {
        return true;
    }

    /**
     * @return integer
     */
    public function getLinkType(){
        return self::InverseMap[$this->link_type];
    }

    public function getLinkTypeString($inverse=false){
        return self::linkTypeToStr($this->link_type, $inverse);
    }

    public function getForeignNames($is_full=false)
    {
        return array_map(function (DataAttribute $da) use ($is_full) {
                return $is_full ? $da->getFullName() : $da->name;
            }, $this->foreign_columns);
    }

    public static function linkTypeToStr($link_type, $inverse=false)
    {
        return $inverse? self::StringMap[self::InverseMap[$link_type]] : self::StringMap[$link_type];
    }

    public static function relationCombine($rel_a, $rel_b, $default=null)
    {
        if(is_null($rel_a) || is_null($rel_b))
            return self::ManyToMany;
        // both 0, 0 as first one is received inverted
        $combined = implode(':', [explode(':', $rel_a)[0], explode(':', $rel_b)[0]]);
        $possiblities = [self::OneToOne, self::OneToMany, self::ManyToOne,
            self::ManyToMany];
        $combined = $combined === "m:m" ? self::ManyToMany : $combined;
        foreach ($possiblities as $possiblity) {
            if($combined === $possiblity)
                return $possiblity;
        }
        return $default;
    }

}

// This is top level where a constraint can be contained.
class DBStore implements \JsonSerializable {
    /**
     * @var boolean
     * is_decoy will be true where db doesn't support schema
     */
    protected $is_decoy;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var SchemaPack[]
     */
    public $schema_packs;

    public $only_schema = null;

    public $only_table = null;

    public function __construct($filter = [])
    {
        $this->only_schema = array_key_exists('schema', $filter) ? $filter['schema'] : null;
        $this->only_table = array_key_exists('table', $filter) ? $filter['table'] : null;
    }

    /**
     * @return bool
     */
    public function isDecoy(): bool
    {
        return $this->is_decoy;
    }

    /**
     * @param bool $is_decoy
     */
    public function setIsDecoy(bool $is_decoy): void
    {
        $this->is_decoy = $is_decoy;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->is_decoy? $this->schema_packs[0]->getName(): $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function process(){
        foreach ($this->schema_packs as $schema_pack) {
            $schema_pack->process();
        }
        return $this;
    }

    /**
     * @param string|integer $constraint_name
     * @param DataAttribute[] $resident_columns
     * @param DataAttribute[] $foreign_columns
     * @param boolean $is_synthetic
     * @return ForeignConstraint
     */
    public function createForeignConstraint($constraint_name, $resident_columns, $foreign_columns, $is_synthetic){
        $resident_entity = $resident_columns[0]->getParent();
        $foreign_entity = $foreign_columns[0]->getParent();

        $jn = implode("_", array_map(function ($column){return $column->name;}, $resident_columns));
        $jnf = implode("_", array_map(function ($column){return $column->name;}, $foreign_columns));
        if($is_synthetic)
            $constraint_name = "synthetic_{$constraint_name}_{$resident_entity->name}_{$jn}_{$jnf}_fkey";

        return $this->createForeignConstraintEx($constraint_name, $resident_columns, $resident_entity,
            $foreign_columns, $foreign_entity, $is_synthetic);

    }

    /**
     * @param string|integer $constraint_name
     * @param DataAttribute[] $resident_columns
     * @param Entity $resident_entity
     * @param DataAttribute[] $foreign_columns
     * @param Entity $foreign_entity
     * @param boolean $is_synthetic
     * @return ForeignConstraint
     */
    public function createForeignConstraintEx($constraint_name, $resident_columns, $resident_entity, $foreign_columns,
                                              $foreign_entity, $is_synthetic){
        $resident_schema = $resident_entity->getParent();
        $foreign_schema = $foreign_entity->getParent();

        $foreign_constraint = new ForeignConstraint(
            $constraint_name, $resident_schema,
            $resident_entity, $resident_columns,
            Constraint::FOREIGN, $foreign_schema,
            $foreign_entity, $foreign_columns);

        $foreign_constraint->is_synthetic = $is_synthetic;
        $resident_entity->addForeign($foreign_constraint);
        return $foreign_constraint;
    }

    /**
     * @return DataAttribute[]
     */
    public function getDataAttributes(){
        $da = [];
        foreach ($this->schema_packs as $schema_pack) {
            $da = array_merge($da, $schema_pack->getDataAttributes());
        }
        return $da;
    }

    /**
     * @return Entity[]
     */
    public function getEntities(){
        $entities = [];
        foreach ($this->schema_packs as $schema_pack) {
            $entities = array_merge($entities, array_values($schema_pack->entities));
        }
        return $entities;
    }

    /**
     * @return Constraint[]
     */
    public function getConstraints(){
        $constraints = [];
        foreach ($this->getEntities() as $entity) {
            $constraints = array_merge($constraints, array_values($entity->constraints));
        }
        return $constraints;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}


