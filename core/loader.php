<?php
/**
 * Author: perilbrain
 */


/**
 * Any loader be it DB based or file based has to implement it.
 */
abstract class LoadConfig {
    public function should_load($config_dir, $parent_dir, $mod_name, $file_name) {
        return TRUE;
    }
    public function getExtensionClass() {
        return 'ExternalPlugin';
    }
}

/**
 *
 */
class PlainLoader extends LoadConfig {
}

/**
 *
 */
class FileConfigLoader extends LoadConfig {
}

/**
 *
 */
class DBConfigLoader extends LoadConfig {
}

/**
 *
 */
class PluginLoader {
    public $base_dirs;
    public $loader;
    public $prefix;
    public $depth = null;
    public $ext_dic = [];

    function __construct(array $base_dirs, LoadConfig $loader, $prefix = "", $depth = 3) {
        $this->base_dirs = $base_dirs;
        $this->loader = $loader;
        $this->prefix = $prefix;
        $this->depth = $depth;
    }

    public function getExtensions() {
        $extensions = [];
        foreach ($this->base_dirs as $bd) {
            $directory = new \RecursiveDirectoryIterator($bd, \FilesystemIterator::FOLLOW_SYMLINKS | \FilesystemIterator::SKIP_DOTS);
            $filter = new \RecursiveCallbackFilterIterator($directory, function ($current, $key, $iterator) use ($bd) {
                if ($current->getFilename()[0] === '.') {return FALSE;}
                if ($current->isDir()) {
                    return TRUE;
                } else {
                    $parent = basename($current->getPath());
                    $file_name = $current->getBasename('.php');
                    $ss = TRUE;
                    if (!empty($this->prefix)) {
                        $ss = substr($file_name, 0, strlen($this->prefix)) === $this->prefix;
                    }
                    $ss = $ss && "{$this->prefix}$parent" === $file_name;
                    return $ss && $this->loader->should_load($bd, $current->getPath(), $parent, $current);
                }
            });
            $iterator = new \RecursiveIteratorIterator($filter);
            if (!is_null($this->depth)) {
                $iterator->setMaxDepth($this->depth);
            }
            $files = array_map(function ($info) {return $info->getPathname();}, iterator_to_array($iterator));
            $extensions = array_merge($extensions, $files);
        }
        return $extensions;
    }

    private function instantiate($extension_file, $extension_class) {
        try {
            $ns = require_once "$extension_file";
            $fc = "$ns\\$extension_class";
            if(!class_exists($fc))
                return null;
            $object = new $fc();
            if (in_array("ILoadablePlugin", class_implements($object))) {
                return $object;
            }
        } catch (Exception $e) {
            echo "$e";
        }
        return null;

    }

    public function loadExtensions($extension_files, $extension_class) {
        foreach ($extension_files as $extension_file) {
            $object = $this->instantiate($extension_file, $extension_class);
            if (!is_null($object)) {
                $this->ext_dic[$object->getName()] = $object;
            }
        }
    }

    public function filterLoadExtensions() {
        $this->loadExtensions($this->getExtensions(), $this->loader->getExtensionClass());
        return $this->ext_dic;
    }

    public function hasPlugin($plugin_name)
    {
        return array_key_exists($plugin_name, $this->ext_dic);
    }

    /**
     * @param string $plugin_name
     * @return ILoadablePlugin
     */
    public function getPlugin($plugin_name)
    {
        if($this->hasPlugin($plugin_name))
            return $this->ext_dic[$plugin_name];
        return null;
    }

    /**
     * @return string[]
     */
    public function getActivePluginNames()
    {
        $active_plugins = [];
        foreach ($this->ext_dic as $value) {
            if($value instanceof DBPlugin)
                if(!$value->isFunctional())
                    continue;
            $active_plugins[] = $value->getName();
        }
        return $active_plugins;
    }

    public static function loadAllPlainPlugins($dirs=[], $prefix="")
    {
        $loader = new PlainLoader();
        $pl = new PluginLoader($dirs, $loader, $prefix);
        $pl->filterLoadExtensions();
        return $pl;
    }
}


/*
function runExtensions() {
    $loader = new PlainLoader();
    $pl = new PluginLoader(['plugged/plugins'], $loader, "plugin_");
    foreach ($pl->filterLoadExtensions() as $key => $obj) {
        echo "Running $key\n";
        $obj->run();
    }
}

runExtensions();


for j in $(ls -d /* | tr -d '/' | grep -vE '[^A-Za-z]' | sort | uniq); do  mkdir -p "$j"; i="${j^}Plug"; cat sample.php | sed -e "s/Sample/$i/" > "$j/plugin_$j.php"; done
 */