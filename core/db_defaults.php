<?php
/**
 * Author: perilbrain
 */

class DBDefaults {
    /**
     * @var string
     */
    protected $db_type;
    /**
     * @var string
     */
    protected $port;
    /**
     * @var string
     */
    protected $server_address;
    /**
     * @var string
     */
    protected $user_name;
    /**
     * @var string
     */
    protected $password;
    /**
     * @var string
     */
    protected $db_prefix;
    /**
     * @var string
     */
    protected $db_schema;
    /**
     * @var string
     */
    protected $selected_db;

    /**
     * DBDefaults constructor.
     * @param string $db_type
     * @param string $port
     * @param string $server_address
     * @param string $user_name
     * @param string $password
     * @param string $db_prefix
     * @param string $db_schema
     * @param string $selected_db
     */
    public function __construct($db_type, $port, $server_address, $user_name, $password, $db_prefix, $db_schema, $selected_db)
    {
        $this->db_type = $db_type;
        $this->port = $port;
        $this->server_address = $server_address;
        $this->user_name = $user_name;
        $this->password = $password;
        $this->db_prefix = $db_prefix;
        $this->db_schema = $db_schema;
        $this->selected_db = $selected_db;
    }

    /**
     * @param string $db_type
     * @param $opts
     * @param string $selected_db
     * @param DBDefaults $def
     * @return DBDefaults
     */
    public static function parse_commands($db_type, $opts, $selected_db, $def){
        $server = !empty($opts['server'])? $opts['server'] : $def->server_address;
        $port = !empty($opts['port'])? $opts['port'] : $def->port;
        $username = !empty($opts['username'])? $opts['username'] : $def->user_name;
        $password = !empty($opts['password'])? $opts['password'] : $def->password;
        $prefix = !empty($opts['prefix'])? $opts['prefix'] : $def->db_prefix;
        $schema = !empty($opts['schema'])? $opts['schema'] : $def->db_schema;
        return new DBDefaults($db_type, $port, $server, $username, $password, $prefix, $schema, $selected_db);
    }

    /**
     * @return string
     */
    public function getDbType()
    {
        return $this->db_type;
    }

    /**
     * @param string $db_type
     * @return DBDefaults
     */
    public function setDbType($db_type)
    {
        $this->db_type = $db_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     * @return DBDefaults
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getServerAddress()
    {
        return $this->server_address;
    }

    /**
     * @param string $server_address
     * @return DBDefaults
     */
    public function setServerAddress($server_address)
    {
        $this->server_address = $server_address;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * @param string $user_name
     * @return DBDefaults
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return DBDefaults
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getDbPrefix()
    {
        return $this->db_prefix;
    }

    /**
     * @param string $db_prefix
     * @return DBDefaults
     */
    public function setDbPrefix($db_prefix)
    {
        $this->db_prefix = $db_prefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getDbSchema()
    {
        return $this->db_schema;
    }

    /**
     * @param string $db_schema
     * @return DBDefaults
     */
    public function setDbSchema($db_schema)
    {
        $this->db_schema = $db_schema;
        return $this;
    }

    /**
     * @return string
     */
    public function getSelectedDB()
    {
        return $this->selected_db;
    }

    /**
     * @param string $selected_db
     * @return DBDefaults
     */
    public function setSelectedDB($selected_db)
    {
        $this->$selected_db = $selected_db;
        return $this;
    }



}