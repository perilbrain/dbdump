<?php

require_once 'db_defaults.php';
/**
 *
 */
abstract class RelDB {

    /**
     * @var DBDefaults $def_db
     */
    protected $def_db;

    protected $bridge = null;

    /**
     * RelDB constructor.
     * @param DBDefaults $def_db
     */
    function __construct($def_db) {
        $this->def_db = $def_db;
    }

    public abstract function connect();

    public abstract function close();

    public function initQuery(){

    }

    public abstract function query($query = '');

    public function warnDisconnect($value = '') {
        print "// ***Database is not connected.\n";
        return false;
    }

    public function normalize_name($value) {
        if (empty($this->def_db->getDbPrefix())) {
            return $value;
        }
        return preg_replace('/^' . preg_quote($this->def_db->getDbPrefix(), '/') . '/', '', $value);
    }

    public abstract function modelDB($synthetic_level=3, $filter=[]);

    /**
     * @param DBStore $db_store
     * @return mixed
     */
    public abstract function getAllSchemaPacks(&$db_store);
    /**
     * @param SchemaPack $schema_pack
     * @return mixed
     */
    public abstract function getAllEntities(&$schema_pack);
    public abstract function getAllAttributes(&$schema_pack, &$entity);
    public abstract function setAllConstraints($resident_name, &$db_store);
    public abstract function discover_synthetic(&$db_store, $synthetic_level);


    /**
     * @param DBStore $db_store
     * @param integer $synthetic_level
     * @param array $valid_types
     * @return array
     */
    protected function synthetic_match(&$db_store, $synthetic_level, $valid_types){

        $da_list = array_filter($db_store->getDataAttributes(), function (DataAttribute $attr_obj) {return $attr_obj->isData();});


        if ($synthetic_level & 1 > 0){

            usort($da_list, function($a, $b) {return strcmp($a->dtype, $b->dtype);});
            $split_arr = [];
            foreach ($da_list as $attr) {
                if(!in_array($attr->dtype, $valid_types)){
                    continue;
                }
                $split_arr[$attr->dtype][] = $attr;
            }
            /**
             * @var ForeignConstraint[] $built_cs
             */
            $built_cs = [];
            // find all who have <table_name>_?id or id_?<table_name> map to table.primary
            // We wont look for composite key now
            foreach ($split_arr as $dtype => $d_attributes) {

                // Collect those whose name has pattern and are not primary.
                $expected_fk = array_filter($d_attributes, function (DataAttribute $da) {
                    return
                        preg_match('/^(id\_?\w+)|(\w+\_?id)$/i', $da->name) !== false && !$da->is_primary;
                }, ARRAY_FILTER_USE_BOTH);

                // Collect primaries separately
                $attribute_pk = array_filter($d_attributes, function ($da) { return $da->is_primary; });

                /**
                 * @var DataAttribute $fk
                 */
                foreach ($expected_fk as $fk) {

                    // Get table_name

                    $expected_table = [];
                    $re_list = ['/(\w+)\_?id/i', '/id\_?(\w+)/i'];
                    // $re = '/(\w+)\_id/';
                    $subs = ['$1s', '$1'];

                    foreach ($subs as $sub) {
                        foreach ($re_list as $re) {
                            $result = preg_replace($re, $sub, $fk->name);
                            $expected_table[] = $result;
                        }
                    }

                    /**
                     * @var DataAttribute $pk
                     */
                    foreach ($attribute_pk as $pk) {
                        // not table as suffixes
                        $any_match = in_array($pk->entity->name, $expected_table) || in_array($pk->entity->tb_name, $expected_table) ;
                        if($any_match && $fk->entity->parent_entity !== $pk->entity && $fk->entity !== $pk->entity){
                            // Link found
                            $built_cs[] = $db_store->createForeignConstraint(1, [$fk], [$pk], true);
                            break;
                        }
                    }

                }

            }

            foreach ($built_cs as $built_c) {
                $built_c->process();
            }
        }

        if ($synthetic_level & 2 > 0){

            // May be use table to table which will be nC2.

            $getAttributesLike = function (array $data_attributes, Entity $entity){
                sort($data_attributes);
                $non_keys = $entity->getNonKeyAttributes();
                sort($non_keys);
                $found = [];
                foreach ($data_attributes as $da) {
                    $got = false;
                    foreach ($non_keys as $non_key) {
                        if(($non_key->name === $da->name) && ($non_key->dtype === $da->dtype)){
                            $found[] = $non_key;
                            $got = true;
                            break;
                        }
                    }
                    if(!$got)
                        return false;
                }
                return $found;
            };

            $all_entities = $db_store->getEntities();
            $combinations = DBUtil::getCombinations($all_entities);
            foreach ($combinations as $combination) {
                /**
                 * @var Entity[] $combination
                 */
                for ($i=0; $i < 2; ++$i){
                    $j = $i^1;
                    $entity1 = $combination[$i];
                    $entity2 = $combination[$j];
                    if($entity1->parent_entity === $entity2 || $entity2->parent_entity === $entity1)
                        break;
                    // try with primary then with uniques
                    $ma = $entity1->primary_keys;
                    $fa = $getAttributesLike($ma, $entity2);
                    if(!empty($fa))
                        $built_cs[] = $db_store->createForeignConstraint(2, $fa, $ma, true);
                    $ma = $entity1->getKeyAttributes(false, true, false);
                    $fa = $getAttributesLike($ma, $entity2);
                    if(!empty($fa))
                        $built_cs[] = $db_store->createForeignConstraint(2, $fa, $ma, true);
                }
            }

            foreach ($built_cs as $built_c) {
                $built_c->process();
            }
        }

    }


}