<?
interface ILoadablePlugin {
	public function getName();
	public function configure($params);
	public function run($params);
	public function clean($params);

    public function help();
}

interface DBPlugin extends ILoadablePlugin{
    public function isFunctional();
}