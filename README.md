## Introduction

DBDump is a utility to generate different formats of database schema along with relations across entities. Currently it supports for postgres

- Hibernate Java Entity code Generator (Supports most of common needs/partition/inheritance/composite keys etc)
- Custom format with relationship finder.

## Status

Project is currently in alpha stage and will take some time to mature. Also for safety, this never attempts to write anything to database. Mysql mode is completely non-functional but will be revamped soon.

## Usage

### Prequisite

- PHP 7.0+ with posgres extension i.e `extension="pgsql.so"`

### Steps

Clone the repository and run 

```sh
php runner.php --help
```

follow the commands as provided in help.

Some examples are

```shell
php runner.php -d pg --schema="my_app" -- my_db
php runner.php -d pg --server="10.2.21.54" --username="postgres" --password="hello_seattle" --schema="my_app" -- my_db
```

will print all hibernate entities for database `my_db` on console. A config file can also be used like

```php
<?php
return [
    'out_dir' => '/tmp/gens',
    'is_console' => false,
    'package' => 'com.test.mypackage',
    'entity_dir' => 'entities',
    'ctrl_dir' => 'controllers',
    'repository_dir' => 'repositories',
    'back_reference' => true,  // To fetch child relation
    'json_ignore' => true,   // if back_reference is false this is useless.
];
// lets say this file is /tmp/config.php
```

And then check the folder `/tmp/gens` after running

```shell
php runner.php -d pg --config=/tmp/config.php -- some_db
```



## Contribution

The code is in pathetic situation. Its very roughly written but is highly customizable in terms of plugins. If anyone is interested in contributing, please do a pull request.

## LICENSE

Project is released under GPLv3.